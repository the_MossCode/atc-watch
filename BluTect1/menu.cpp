
#include "menu.h"
#include "class.h"
#include "images.h"
#include "pinout.h"
#include "touch.h"
#include "backlight.h"
#include "bootloader.h"
#include "display.h"
#include "menu_Boot.h"
#include "menu_Home.h"
#include "menu_Heart.h"
#include "menu_Debug.h"
#include "menu_Reboot.h"
#include "menu_Update.h"
#include "menu_Off.h"
#include "menu_Notify.h"
#include "menu_Battery_Notify.h"
#include "menu_Settings_Time.h"
#include "menu_Settings_Date.h"
#include "menu_Settings_Brightness.h"
#include "Menu_Antiloss.h"
#include "menu_Steps.h"
#include "menu_App.h"
#include "menu_AntilossPush.h"
#include "menu_Charging.h"
#include "menu_SafeList.h"
#include "menu_Alarm_Notify.h"
#include "menu_Alarm.h"
#include "menu_Settings.h"
#include "menu_blepush.h"
#include "menu_about.h"

#include "menu_BLEDetected.h"
#include "menu_notifications.h"
#include "notification_img.h"

#include <lvgl.h> 

#define MENU_NUM      3

long last_main_run;
int vars_menu = -1;
int vars_max_menu = MENU_NUM - 1;
bool swipe_enabled_bool = false;

Screen *currentScreen = &homeScreen;
Screen *oldScreen = &homeScreen;
Screen *lastScreen = &homeScreen;

app_struct heartApp = {"HeartRate", &heart, &heartScreen};
app_struct settingsApp = {"Settings", &img_settings, &settingsScreen};
app_struct acclApp = {"Steps", &footprints , &stepsScreen};
app_struct safelistApp = {"Safe List", &img_safe_list , &safeListScreen};
app_struct alarmApp = {"Alarm", &img_alarm , &alarmScreen};
app_struct antilossApp = {"Anti Loss", &img_antiloss, &antiLossScreen};
#if SHOW_NOTIFICATION
app_struct notificationApp = {"Messages", &notification_img, &notificationsScreen};
#endif // SHOW_NOTIFICATION
app_struct aboutApp = {"About", &img_about, &aboutScreen};
#if DEBUG
app_struct debugApp = {"DEBUG", NULL, &debugScreen};
#endif

int maxApps = 2;
AppScreen apps1Screen(1, maxApps, &heartApp, &acclApp, &safelistApp , &settingsApp);
#if DEBUG
AppScreen apps2Screen(2, maxApps, &alarmApp, &antilossApp ,
#if SHOW_NOTIFICATION
&notificationApp 
#else
NULL
#endif // SHOW_NOTIFICATION 
 , &debugApp);
#else
AppScreen apps2Screen(2, maxApps, &alarmApp, &antilossApp , 
#if SHOW_NOTIFICATION
&notificationApp 
#else
NULL
#endif // SHOW_NOTIFICATION
, &aboutApp);
#endif

Screen *menus[MENU_NUM] = {&homeScreen, &apps1Screen, &apps2Screen};

void init_menu() {

}

void display_home() {
  lastScreen = currentScreen;
  currentScreen = &homeScreen;
  vars_menu = 0;
}

void display_notify() {
  lastScreen = currentScreen;
  currentScreen = &notifyScreen;
  vars_menu = 0;
}

void display_charging() {
  lastScreen = currentScreen;
  currentScreen = &chargingScreen;
  vars_menu = 0;
}

void display_booting() {
  set_gray_screen_style();
  lastScreen = currentScreen;
  currentScreen = &bootScreen;
  oldScreen = &bootScreen;
  set_swipe_enabled(false);
  currentScreen->pre_display();
  set_gray_screen_style();
  currentScreen->pre();
  currentScreen->main();
  inc_tick();
  lv_task_handler();
}

void display_screen(bool ignoreWait) {
  if (ignoreWait || millis() - last_main_run > get_menu_delay_time()) {
    last_main_run = millis();
    if (currentScreen != oldScreen) {
      oldScreen->post();
      currentScreen->pre_display();
      set_gray_screen_style();
      oldScreen = currentScreen;
      set_swipe_enabled(false);
      currentScreen->pre();
    }
    currentScreen->main();
  }
  lv_task_handler();
}

void display_bledetected()
{
  change_screen(&bleDetectedScreen);
}

void check_menu(touch_data_struct touch_data) {
  if (touch_data.gesture == TOUCH_SLIDE_UP) {
    currentScreen->up();
  } else if (touch_data.gesture == TOUCH_SLIDE_DOWN) {
    currentScreen->down();
  } else if (touch_data.gesture == TOUCH_SINGLE_CLICK) {
    currentScreen->click(touch_data);
  } else if (touch_data.gesture == TOUCH_LONG_PRESS) {
    currentScreen->long_click(touch_data);
  } else if (touch_data.gesture == TOUCH_SLIDE_LEFT) {
    currentScreen->left();
  } else if (touch_data.gesture == TOUCH_SLIDE_RIGHT) {
    currentScreen->right();
  }
}

uint32_t get_menu_delay_time() {
  return currentScreen->refreshTime();
}

void change_screen(Screen* screen) {
  if(currentScreen != screen){
    lastScreen = currentScreen;
    currentScreen = screen;
  }
}

int get_sleep_time_menu() {
  return currentScreen->sleepTime();
}

void set_last_menu() {
  currentScreen = lastScreen;
}

void set_swipe_enabled(bool state) {
  swipe_enabled_bool = state;
}

bool swipe_enabled() {
  return swipe_enabled_bool;
}

void inc_vars_menu() {
  lastScreen = currentScreen;
  vars_menu++;
  if (vars_menu > vars_max_menu || vars_menu < 0)vars_menu = 0;
  currentScreen = menus[vars_menu];
}

void dec_vars_menu() {
  lastScreen = currentScreen;
  vars_menu--;
  if (vars_menu < 0 || vars_menu > vars_max_menu)vars_menu = vars_max_menu;
  currentScreen = menus[vars_menu];
}

static void lv_event_handler(lv_obj_t * object, lv_event_t event)
{
  currentScreen->lv_event_class(object, event);
}

void display_wakeup_alarm_notification()
{
  change_screen(&alarmNotifyScreen);
}

void display_battery_notify()
{
	change_screen(&batteryNotifyScreen);
}

void display_antiloss_notify()
{
	change_screen(&antilossPushScreen);
}

void display_ble_disconnect_push()
{
  change_screen(&blePushScreen);
}