
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

class SettingsBrightnessScreen : public Screen
{
public:
  virtual void pre()
  {
    set_gray_screen_style();
    set_swipe_enabled(true);

    backlight_brightness = get_backlight();

    label_screen = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_screen, "Set Brightness");
    lv_obj_align(label_screen, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);

    slider = lv_slider_create(lv_scr_act(), NULL);
    lv_slider_set_range(slider, 1, 7);
    lv_obj_set_event_cb(slider, lv_event_handler);
    lv_obj_align(slider, NULL, LV_ALIGN_CENTER, 0, -10);
    lv_slider_set_value(slider, get_backlight(), LV_ANIM_OFF);

    slider_label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text_fmt(slider_label, "%u", lv_slider_get_value(slider));
    lv_obj_set_auto_realign(slider_label, true);
    lv_obj_align(slider_label, slider, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);
  }

  virtual void right()
  {
    set_last_menu();
  }

  virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
  {
    if (object == slider && event == LV_EVENT_VALUE_CHANGED)
    {
      int slider_backlight = lv_slider_get_value(slider);
      lv_label_set_text_fmt(slider_label, "%u", slider_backlight);
      set_backlight(slider_backlight);
    }
  }

private:
  int backlight_brightness;
  lv_obj_t *label_screen;
  lv_obj_t *slider, *slider_label;
};

SettingsBrightnessScreen settingsBrightnessScreen;
