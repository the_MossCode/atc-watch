#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "inputoutput.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"
#include "fonts.h"
#include <lvgl.h>

#include "antiloss.h"

class HomeScreen : public Screen
{
public:
  virtual void pre()
  {
    time_data = get_time();
    accl_data = get_accl_data();

    set_gray_screen_style();

    lv_style_copy(&st, &lv_style_plain);
    st.text.color = BLUTECT_COLOR_MILK;
    st.text.font = &mksd_medium;

    lv_style_copy(&st_date, &st);
    st_date.text.font = &pt_sans_narrow_reg;

    label_time = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text_fmt(label_time, "%02i:%02i", time_data.hr, time_data.min);
    lv_obj_set_style(label_time, &st);
    lv_obj_align(label_time, NULL, LV_ALIGN_CENTER, 0, -40);
    label_date = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text_fmt(label_date, "%02i/%02i/%04i", time_data.day, time_data.month, time_data.year);
    lv_obj_set_style(label_date, &st_date);
    lv_obj_align(label_date, NULL, LV_ALIGN_CENTER, 0, 0);

    last_batt_percent = 0;
    temp_batt_dsc = batt_0;
    img_batt = lv_img_create(lv_scr_act(), NULL);
    lv_obj_align(img_batt, NULL, LV_ALIGN_IN_TOP_LEFT, (240-45), 5);
    lv_img_set_src(img_batt, &temp_batt_dsc);

    img_ble = lv_img_create(lv_scr_act(), NULL);
    lv_obj_align(img_ble, NULL, LV_ALIGN_IN_TOP_LEFT, 5, 5);
    if (get_vars_ble_connected() == ble_connected)
    {
      lv_img_set_src(img_ble, &ble_blue);
    }
    else
    {
      lv_img_set_src(img_ble, &ble_red);
    }
    last_ble_state = get_vars_ble_connected();

    img_antiloss = lv_img_create(lv_scr_act(), NULL);
    lv_obj_align(img_antiloss, NULL, LV_ALIGN_IN_TOP_LEFT, 108, 5);
    if (antiloss_get_state() != ANTILOSS_STATE_CONNECTED && antiloss_get_state() >= ANTILOSS_STATE_CONNECT)
    {
      lv_img_set_src(img_antiloss, &red_x);
    }
    else if (antiloss_get_state() == ANTILOSS_STATE_CONNECTED)
    {
      lv_img_set_src(img_antiloss, &green_0);
    }
    else{
      lv_obj_set_hidden(img_antiloss, true);
    }
    last_antiloss_state = antiloss_get_state();

    img_heart = lv_img_create(lv_scr_act(), NULL);
    lv_img_set_src(img_heart, &heart);
    lv_obj_align(img_heart, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, -40);

    label_heart = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(label_heart, 240);
    lv_obj_set_style(label_heart, &st_date);
    lv_label_set_text_fmt(label_heart, "%i", get_last_heartrate());
    lv_obj_align(label_heart, img_heart, LV_ALIGN_OUT_RIGHT_MID, 2, 0);

    img_steps = lv_img_create(lv_scr_act(), NULL);
    lv_img_set_src(img_steps, &footprints);
    lv_obj_align(img_steps, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 120, -40);

    label_steps = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(label_steps, 240);
    lv_obj_set_style(label_steps, &st_date);
    lv_label_set_text_fmt(label_steps, "%i", accl_data.steps);
    lv_obj_align(label_steps, img_steps, LV_ALIGN_OUT_RIGHT_MID, 2, 0);

    label_msg = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_align(label_msg, LV_LABEL_ALIGN_CENTER);
    label_msg_st = lv_label_create(lv_scr_act(), label_msg);
    lv_label_set_text(label_msg_st, ": ");
    lv_label_set_text(label_msg, string2char(get_push_msg(30)));
    lv_obj_align(label_msg_st, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, -12);
    lv_obj_align(label_msg, label_msg_st, LV_ALIGN_OUT_RIGHT_MID, 2, 0);
  }

  virtual void main()
  {
    time_data = get_time();
    accl_data = get_accl_data();
    char time_string[14];
    sprintf(time_string, "%02i:%02i", time_data.hr, time_data.min);
    char date_string[14];
    sprintf(date_string, "%02i/%02i/%04i", time_data.day, time_data.month, time_data.year);

    lv_label_set_text(label_time, time_string);
    lv_label_set_text(label_date, date_string);

    lv_label_set_text_fmt(label_heart, "%i", get_last_heartrate());
    lv_label_set_text_fmt(label_steps, "%i", accl_data.steps);

    if (last_ble_state != get_vars_ble_connected())
    {
      if (get_vars_ble_connected() == ble_connected)
      {
        lv_img_cache_invalidate_src(&ble_red);
        lv_img_set_src(img_ble, &ble_blue);
        last_ble_state = get_vars_ble_connected();
      }
      else
      {
        lv_img_cache_invalidate_src(&ble_blue);
        lv_img_set_src(img_ble, &ble_red);
        last_ble_state = get_vars_ble_connected();
      }
    }

    if (antiloss_get_state() < ANTILOSS_STATE_CONNECT)
    {
      lv_obj_set_hidden(img_antiloss, true);
    }
    else
    {
      if (last_antiloss_state != antiloss_get_state())
      {
        last_antiloss_state = antiloss_get_state();
        if (antiloss_is_device_lost())
        {
          lv_obj_set_hidden(img_antiloss, false);
          lv_img_cache_invalidate_src(&green_0);
          lv_img_set_src(img_antiloss, &red_x);
        }
        else
        {
          lv_obj_set_hidden(img_antiloss, false);
          lv_img_cache_invalidate_src(&red_x);
          lv_img_set_src(img_antiloss, &green_0);
        }
      }
    }

    batt_update();
  }

  virtual void up()
  {
    inc_vars_menu();
  }

  virtual void down()
  {
    dec_vars_menu();
  }

  virtual void left()
  {
  }

  virtual void right()
  {
  }

private:
  time_data_struct time_data;
  accl_data_struct accl_data;

  lv_style_t st, st_date;

  lv_obj_t *label_heart, *label_steps, *label_msg, *label_msg_st;
  lv_obj_t *label_time, *label_date;

  lv_obj_t *img_heart, *img_steps;

  lv_obj_t *img_ble;
  byte last_ble_state;

  lv_obj_t *img_batt;
  lv_img_dsc_t temp_batt_dsc;
  unsigned int last_batt_percent;

  lv_obj_t *img_antiloss;
  unsigned int last_antiloss_state;

  char *string2char(String command)
  {
    if (command.length() != 0)
    {
      char *p = const_cast<char *>(command.c_str());
      return p;
    }
  }

  void batt_update()
  {
    unsigned int curr_batt_percent = get_battery_percent();
    if ((last_batt_percent <= 80) && (curr_batt_percent > 80))
    {
      lv_img_cache_invalidate_src(&temp_batt_dsc);
      temp_batt_dsc = batt_100;
      lv_img_set_src(img_batt, &temp_batt_dsc);
    }
    else if ((last_batt_percent > 80 || last_batt_percent <= 60) && (curr_batt_percent <= 80 && curr_batt_percent > 60))
    {
      lv_img_cache_invalidate_src(&temp_batt_dsc);
      temp_batt_dsc = batt_75;
      lv_img_set_src(img_batt, &temp_batt_dsc);
    }
    else if ((last_batt_percent > 60 || last_batt_percent <= 40) && (curr_batt_percent <= 60 && curr_batt_percent > 40))
    {
      lv_img_cache_invalidate_src(&temp_batt_dsc);
      temp_batt_dsc = batt_50;
      lv_img_set_src(img_batt, &temp_batt_dsc);
    }
    else if ((last_batt_percent > 40 || last_batt_percent <= 20) && (curr_batt_percent <= 40 && curr_batt_percent > 20))
    {
      lv_img_cache_invalidate_src(&temp_batt_dsc);
      temp_batt_dsc = batt_25;
      lv_img_set_src(img_batt, &temp_batt_dsc);
    }
    else if ((last_batt_percent > 20) && (curr_batt_percent <= 20))
    {
      lv_img_cache_invalidate_src(&temp_batt_dsc);
      temp_batt_dsc = batt_0;
      lv_img_set_src(img_batt, &temp_batt_dsc);
    }
    last_batt_percent = curr_batt_percent;
  }
};

HomeScreen homeScreen;
