
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"


class BatteryNotifyScreen : public Screen
{
	public:
	virtual void pre()
	{
		set_gray_screen_style();
		
		label = lv_label_create(lv_scr_act(), NULL);
		lv_label_set_text(label, "BATTERY LOW!");
		lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
		lv_obj_align(label, NULL, LV_ALIGN_CENTER, 0, 0);
	}

	virtual void main()
	{
		
	}

	virtual void right()
	{
		display_home();
	}

	virtual uint32_t sleepTime()
	{
		return 5000;
	}
	
	private:
	lv_obj_t *label;
};

BatteryNotifyScreen batteryNotifyScreen;
