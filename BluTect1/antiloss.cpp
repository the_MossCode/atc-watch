/*
 * antilosscpp.cpp
 *
 * Created: 18/09/2020 20:23:41
 *  Author: collo
 */ 

#include "antiloss.h"
#include "push.h"
#include "ble_central.h"

#define LOCAL_CONNECT_TIMEOUT	10000
#define DISCONNECT_TIMEOUT		10000
#define RECONNECTION_TIME		5000

BLERemoteService immediate_alert = BLERemoteService("1802");
BLERemoteCharacteristic alert_level = BLERemoteCharacteristic("2A06", 0x04);// Write no response

enum connecting_states{
connecting_state_init, connecting_state_retry, connecting_state_disconnect, connecting_state_connect
};

enum disconnecting_states{
	disconnecting_state_init, disconnecting_state_disconnect, disconnecting_state_retry
};

typedef struct
{
	uint32_t reconn_millis;
	uint8_t reconn_attempts;
	uint32_t lost_millis;
	uint8_t lost_recon_attempts;
}retry_timers_t;


ble_peer_device_t antiloss_peer;
bool is_delete_antiloss_peer = false;
bool is_device_lost = false;
uint8_t antiloss_notify_state = ALOSS_NOTIFICATION_NONE;
uint8_t antiloss_state = ANTILOSS_STATE_IDLE;
uint8_t antiloss_gatt_state = antiloss_gatt_discover_att;

retry_timers_t retry_timers = {millis(), 0, millis(), 0};
bool chr_written = false;

static void antiloss_notify_loop();
static void antiloss_connect_loop();
static void antiloss_connecting();
static void antiloss_connected_loop();
static void antiloss_disconnect_loop();
static void antiloss_disconnected_loop();
static void antiloss_lost_loop();
static void antiloss_del_loop();

void antiloss_init()
{
	memset(&antiloss_peer.peer_address, 0x00, sizeof(ble_gap_addr_t));
	memset(antiloss_peer.local_name, 0x00, LOCAL_NAME_MAX_SIZE);
	
	antiloss_peer.list_type = PEER_LIST_TYPE_NONE;
	antiloss_peer.local_name_len = 0;
	antiloss_peer.rssi = 0;
	
	antiloss_set_notify(ALOSS_NOTIFICATION_NONE);
	antiloss_set_state(ANTILOSS_STATE_DISABLED);

	ble_central_add_remote_att(immediate_alert);
	ble_central_add_remote_att(alert_level);

	alert_level.setEventHandler(BLEValueUpdated, antiloss_gatt_write_chr_callback);

	// todo add characteristic callback
}

uint8_t antiloss_get_notify()
{
	return antiloss_notify_state;
}

void antiloss_set_notify(uint8_t notify)
{
	antiloss_notify_state = notify;
}

uint8_t antiloss_get_state()
{
	return antiloss_state;
}

void antiloss_set_state(uint8_t state)
{	
	antiloss_state = state;
}

uint8_t antiloss_get_peer_device_type()
{
	return antiloss_peer.list_type;
}

bool antiloss_is_device_lost()
{
	return is_device_lost;
}

char *antiloss_get_device_name()
{
	return antiloss_peer.local_name;
}

void antiloss_gatt_attribute_discover_callback()
{
	antiloss_gatt_state = antiloss_gatt_enable_notifications;
}

void antiloss_gatt_write_chr_callback(BLECentral& central, BLERemoteCharacteristic& chr)
{
	antiloss_gatt_state = antiloss_gatt_complete;
	chr_written = false;
}

void antiloss_call_device()
{
	if(alert_level.canWrite()){
		const unsigned char alert_level_1[] = {0x01};
		if(alert_level.write(alert_level_1, 1)){
			BLE_DEBUG_PRINT("AL_1");
			chr_written = true;
		}
	}
}

uint8_t antiloss_gatt_get_state()
{
	return antiloss_gatt_state;
}

BLERemoteService *antiloss_gatt_immediate_alert_svc()
{
	return &immediate_alert;
}

BLERemoteCharacteristic *antiloss_gatt_alert_chr()
{
	return &alert_level;
}

void antiloss_disable()
{
	//todo Disconnect if already connected
	//todo delete existing devices
	antiloss_state = ANTILOSS_STATE_DISABLED;
}

void antiloss_enable()
{
	antiloss_state = ANTILOSS_STATE_IDLE;
}

void antiloss_add_device(ble_peer_device_t* device)
{	
	antiloss_peer = *device;
}

ble_peer_device_t *antiloss_get_device()
{
	return &antiloss_peer;
}

void antiloss_delete_device()
{
	if(antiloss_state == ANTILOSS_STATE_CONNECTED){
		antiloss_state = ANTILOSS_STATE_DISCONNECT;
		is_delete_antiloss_peer = true;
	}
	else{
		memset(&antiloss_peer.peer_address, 0x00, sizeof(ble_gap_addr_t));
		is_delete_antiloss_peer = false;
		is_device_lost = false;
		ble_write("AT+TAG:3");
		antiloss_set_notify(ALOSS_NOTIFICATION_NONE);
		antiloss_set_state(ANTILOSS_STATE_IDLE);

		BLE_DEBUG_PRINT("DEL: COMPLETE");
	}
}

static void antiloss_notify_loop()
{
	if (antiloss_notify_state == ALOSS_NOTIFY){
		show_antiloss_push(false);
	}
}

static void antiloss_connect_loop()
{
	// timeout after x seconds and declare lost
	static uint8_t connecting_state = connecting_state_init;
	static uint32_t connect_timer_millis = millis();
	static uint8_t connection_retries = 0;
	uint32_t connect_res = NRF_SUCCESS;

	switch (connecting_state)
	{
	case connecting_state_init:{
		connect_res = ble_central_connect(antiloss_peer);
		if(connect_res == NRF_SUCCESS){
			connecting_state = connecting_state_connect;
			connection_retries = 0;
		}
		else if (connect_res == NRF_ERROR_CONN_COUNT || connect_res == NRF_ERROR_RESOURCES){
			connecting_state = connecting_state_disconnect; // Too many connections, Disconnect device
		}
		else if(connect_res == NRF_ERROR_BUSY){
			connecting_state = connecting_state_retry;
		}
		else{
			connection_retries = 0;
			BLE_DEBUG_PRINT("CE_" + String(connect_res, HEX));
			antiloss_set_state(ANTILOSS_STATE_DISCONNECTED);
		}
		connect_timer_millis = millis();
		break;
	}
	case connecting_state_retry:{
		// attempt reconnection after 1 second
		if(millis() - connect_timer_millis >= 1000){
			connecting_state = connecting_state_init;
			connection_retries++;
			connect_timer_millis = millis();
		}

		if(connection_retries >= 2){
			connection_retries = 0;
			antiloss_set_state(ANTILOSS_STATE_DISCONNECTED);
		}
		break;
	}
	case connecting_state_disconnect:{
		uint32_t disconn_res = ble_central_disconnect(NULL);
		if(disconn_res == NRF_SUCCESS){
			connecting_state = connecting_state_init;
		}
		else if(disconn_res == NRF_ERROR_INVALID_STATE){
			if(millis() - connect_timer_millis >= 3000){
				connecting_state = connecting_state_retry;
				connect_timer_millis = millis();
			}
		}
		else{
			connecting_state = connecting_state_retry;
			connect_timer_millis = millis();
		}
		break;
	}
	default:{
		if(millis() - connect_timer_millis >= 5000){
			BLE_DEBUG_PRINT("AR_1");
			uint32_t cancel_res = ble_central_cancel_connect();
			if(cancel_res == NRF_SUCCESS){
				connecting_state = connecting_state_retry;
			}
			else if(cancel_res == NRF_ERROR_INVALID_STATE){
				if(millis() - connect_timer_millis > 8000){
					connecting_state = connecting_state_retry;
				}
			}
			else{
				antiloss_set_state(ANTILOSS_STATE_DISCONNECTED);
			}
		}
		break;
	}
	}

	// if(connecting_state == 0){
	// 	connect_res = ble_central_connect(&antiloss_peer);
	// 	if(connect_res == NRF_SUCCESS || connect_res == NRF_ERROR_INVALID_STATE || connect_res == NRF_ERROR_BUSY){
	// 		BLE_DEBUG_PRINT("CON: " + String(connect_res));
	// 		connecting_state++;
	// 	}
	// 	else if(connect_res == NRF_ERROR_CONN_COUNT || connect_res == NRF_ERROR_RESOURCES){
	// 		BLE_DEBUG_PRINT("CON: " + String(connect_res));
	// 		set_antiloss_state(ANTILOSS_STATE_DISCONNECT);
	// 		return;
	// 	}
	// 	else{
	// 		BLE_DEBUG_PRINT("CON: " + String(connect_res));
	// 		set_antiloss_state(ANTILOSS_STATE_DISCONNECTED);
	// 		return;
	// 	}
	// }

	// if(millis() - op_timer >= LOCAL_CONNECT_TIMEOUT){
	// 	if(connecting_state > 0){
	// 		connect_res = ble_central_cancel_connection();
	// 		if(connect_res == NRF_SUCCESS || connect_res == NRF_ERROR_INVALID_STATE){
	// 			connecting_state = 0;
	// 			set_antiloss_state(ANTILOSS_STATE_DISCONNECTED);
	// 			BLE_DEBUG_PRINT("CON: TIMEOUT");
	// 		}
	// 	}
	// 	else{
	// 		set_antiloss_state(ANTILOSS_STATE_DISCONNECTED);
	// 		BLE_DEBUG_PRINT("CON: TIMEOUT");
	// 	}
	// }
}

static void antiloss_connected_loop()
{
	// Device connected, discover services
	switch(antiloss_gatt_state){
		case antiloss_gatt_discover_att:{
			break;
		}
		case antiloss_gatt_enable_notifications:{
			antiloss_gatt_state = antiloss_gatt_complete;
			break;
		}
		case antiloss_gatt_call:{
			if(!chr_written && alert_level.canWrite()){
				const unsigned char alert_level_1[] = {0x01};
				if(alert_level.write(alert_level_1, 1)){
					chr_written = true;
					BLE_DEBUG_PRINT("AL_1R");
				}
			}
			break;
		}
		default:{
			break;
		}
	}
}

static void antiloss_disconnect_loop()
{
	// todo timeout after 30 seconds
	static uint8_t disconnect_state = disconnecting_state_init;
	static uint32_t disconn_timer_millis = millis();
	static uint8_t disconn_retries = 0;

	switch (disconnect_state)
	{
	case disconnecting_state_init:{
		uint32_t disconnect_res = ble_central_disconnect(NULL);
		if(disconnect_res == NRF_SUCCESS){
			disconnect_state = disconnecting_state_disconnect;
		}
		else if(disconnect_res == NRF_ERROR_INVALID_STATE){
			disconnect_state = disconnecting_state_retry;
		}
		else{
			antiloss_delete_device();
		}
		disconn_timer_millis = millis();
		break;
	}
	case disconnecting_state_retry:{
		if(millis() - disconn_timer_millis >= 3000){
			disconn_retries++;
			disconnect_state = disconnecting_state_init;
		}

		if(disconn_retries >= 5){
			antiloss_delete_device();
		}
		break;
	}
	default:{
		break;
	}
	}
}

static void antiloss_disconnected_loop()
{
	if(is_delete_antiloss_peer){
		antiloss_delete_device();
		return;
	}

	if(millis() - retry_timers.reconn_millis >= 3000){
		antiloss_set_state(ANTILOSS_STATE_CONNECT);
		retry_timers.reconn_attempts++;
		retry_timers.reconn_millis  = millis();
	}

	if(retry_timers.reconn_attempts >= 5){
		retry_timers.reconn_attempts = 0;
		if(antiloss_notify_state != ALOSS_NOTIFY_DISABLE){
			antiloss_notify_state = ALOSS_NOTIFY;
		}
		antiloss_set_state(ANTILOSS_STATE_LOST);
	}
}

static void antiloss_lost_loop()
{
	if(millis() - retry_timers.lost_millis > 60000){
		antiloss_set_state(ANTILOSS_STATE_CONNECT);
		retry_timers.lost_millis = millis();

		BLE_DEBUG_PRINT("LOST_R: " + String(retry_timers.lost_millis/60000));
	}
}

static void antiloss_del_loop()
{
	antiloss_disconnect_loop();
}

void antiloss_loop()
{
	antiloss_notify_loop();

	switch(antiloss_state){
		case ANTILOSS_STATE_DISABLED:{
			break;
		}
		case ANTILOSS_STATE_IDLE:{
			break;
		}
		case ANTILOSS_STATE_CONNECT:{
			antiloss_connect_loop();
			break;
		}
		case ANTILOSS_STATE_CONNECTED:{
			if(millis() - retry_timers.lost_millis > 10000){
				is_device_lost = false;
			}

			antiloss_connected_loop();
			break;
		}
		case ANTILOSS_STATE_DISCONNECT:{
			antiloss_disconnect_loop();
			break;
		}
		case ANTILOSS_STATE_DISCONNECTED:{
			antiloss_disconnected_loop();
			antiloss_gatt_state = antiloss_gatt_discover_att;
			break;
		}
		case ANTILOSS_STATE_LOST:{
			antiloss_lost_loop();
			break;
		}
		case ANTILOSS_STATE_DEL_DEVICE:{
			antiloss_del_loop();
			break;
		}
		default:{
			break;
		}
	}
}

