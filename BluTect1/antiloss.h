/*
 * antiloss.h
 *
 * Created: 18/09/2020 20:21:01
 *  Author: collo
 */ 


#ifndef ANTILOSS_H_
#define ANTILOSS_H_

#include "ble.h"

enum ble_antiloss_state{
	ANTILOSS_STATE_DISABLED, 
	ANTILOSS_STATE_IDLE,
	ANTILOSS_STATE_CONNECT,
	ANTILOSS_STATE_CONNECTED, 
	ANTILOSS_STATE_DISCONNECT,
	ANTILOSS_STATE_DISCONNECTED,
	ANTILOSS_STATE_LOST,
	ANTILOSS_STATE_DEL_DEVICE
};

enum aloss_notification_state{
    ALOSS_NOTIFICATION_NONE, 
    ALOSS_NOTIFY,
	ALOSS_NOTIFY_DISABLE
};

enum antiloss_gatt_states{
	antiloss_gatt_discover_att, antiloss_gatt_enable_notifications, antiloss_gatt_call, antiloss_gatt_complete
};

void antiloss_init();

uint8_t antiloss_get_notify();
void antiloss_set_notify(uint8_t notify);
uint8_t antiloss_get_state();
void antiloss_set_state(uint8_t state);
uint8_t antiloss_get_connect_state();
void antiloss_set_connect_state(uint8_t state);
uint8_t antiloss_get_peer_device_type();
bool antiloss_is_device_lost();

char *antiloss_get_device_name();

void antiloss_gatt_attribute_discover_callback();
void antiloss_gatt_write_chr_callback(BLECentral& central, BLERemoteCharacteristic& chr);
void antiloss_call_device();
uint8_t antiloss_gatt_get_state();

BLERemoteService *antiloss_gatt_immediate_alert_svc();
BLERemoteCharacteristic *antiloss_gatt_alert_chr();

void antiloss_disable();
void antiloss_enable();

void antiloss_reset_op_timer();
bool antiloss_operation_timed_out(unsigned long timeout);

void antiloss_add_device(ble_peer_device_t* device);
ble_peer_device_t *antiloss_get_device();

void antiloss_delete_device();
void antiloss_loop();

#endif /* ANTILOSS_H_ */