
#include "Arduino.h"
#include "sleep.h"
#include "menu.h"
#include "display.h"
#include "inputoutput.h"
#include "notifications.h"
#include "push.h"

//#include "menu_notifications.h"

String msgText = " ";
String httpText = " ";

struct dnd_t
{
	bool enabled;
	byte source;
};

struct dnd_t dnd = {false, DND_SOURCE_NONE};


void init_push()
{
}

String filter_string(String str)
{
	int i = 0, len = str.length();
	while (i < len)
	{
		char c = str[i];
		if ((c >= 0x20))
		{
			++i;
		}
		else
		{
			str.remove(i, 1);
			--len;
		}
	}
	return str;
}

void show_push(String pushMSG)
{
	if(do_not_disturb()){
		return;
	}

	int commaIndex = pushMSG.indexOf(',');
	int secondCommaIndex = pushMSG.indexOf(',', commaIndex + 1);
	int lastCommaIndex = pushMSG.indexOf(',', secondCommaIndex + 1);
	String MsgText = pushMSG.substring(commaIndex + 1, secondCommaIndex);
	int timeShown = pushMSG.substring(secondCommaIndex + 1, lastCommaIndex).toInt();
	int SymbolNr = pushMSG.substring(lastCommaIndex + 1).toInt();
	msgText = filter_string(MsgText);

	if(!add_notification_to_buffer(msgText)){
		return;
	}

	sleep_up(WAKEUP_BLEPUSH);
	display_notify();
	set_motor_ms();
	set_led_ms(100);
	set_sleep_time();
}

void show_push(String pushMSG, bool vibrate)
{
	if(do_not_disturb()){
		return;
	}


	int commaIndex = pushMSG.indexOf(',');
	int secondCommaIndex = pushMSG.indexOf(',', commaIndex + 1);
	int lastCommaIndex = pushMSG.indexOf(',', secondCommaIndex + 1);
	String MsgText = pushMSG.substring(commaIndex + 1, secondCommaIndex);
	int timeShown = pushMSG.substring(secondCommaIndex + 1, lastCommaIndex).toInt();
	int SymbolNr = pushMSG.substring(lastCommaIndex + 1).toInt();
	msgText = filter_string(MsgText);
	sleep_up(WAKEUP_BLEPUSH);
	display_notify();
	if(vibrate){
		set_motor_ms();
		set_led_ms(100);
	}
	set_sleep_time();
}

bool do_not_disturb()
{
	return dnd.enabled;
}

void enable_do_not_disturb(bool enable, byte source)
{
	if(enable){
		dnd.enabled = enable;
		dnd.source = source;

		set_display_handsup(false);
	}
	else{
		if(dnd.source != source){
			if(source == DND_SOURCE_USER){// User is always allowed to overwrite
				dnd.enabled = enable;
			}
		}
		else{// Same source that enabled is requesting a disable
			dnd.enabled = enable;
			dnd.source = source;
			set_display_handsup(true);
		}
	}
}

void show_http(String httpMSG)
{
	if(do_not_disturb()){
		return;
	}
	httpText = filter_string("http: " + httpMSG);
	set_motor_ms();
	set_sleep_time();
}

String get_http_msg(int returnLength)
{
	if (returnLength != 0 || httpText.length() == returnLength)
	{
		if (httpText.length() < returnLength)
		{
			String tempText = httpText;
			int toSmall = returnLength - httpText.length();
			for (int i = 0; i < toSmall; i++)
			{
				tempText += " ";
			}
			return tempText;
		}
		else if (httpText.length() > returnLength)
		return httpText.substring(0, returnLength - 3) + "...";
	}
	return httpText;
}

String get_push_msg(int returnLength)
{
	if (returnLength != 0 || msgText.length() == returnLength)
	{
		if (msgText.length() < returnLength)
		{
			String tempText = msgText;
			int toSmall = returnLength - msgText.length();
			for (int i = 0; i < toSmall; i++)
			{
				tempText += " ";
			}
			return tempText;
		}
		else if (msgText.length() > returnLength)
		return msgText.substring(0, returnLength - 3) + "...";
	}
	return msgText;
}


void show_ble_detected(bool wake_up)
{
	if(do_not_disturb()){
		return;
	}

	if (wake_up)
	{
		sleep_up(WAKEUP_BLEDETECTED);
		set_motor_ms(1000);
		set_sleep_time();
	}

	display_bledetected();
}

void show_alarm_push(bool vibrate)
{	
	if(vibrate){
		sleep_up(WAKEUP_ALARM);
		set_motor_ms(1000);
		set_sleep_time();
	}

	display_wakeup_alarm_notification();
}

void show_low_battery_push()
{	
	sleep_up(WAKEUP_BATTERYLOW);
	set_motor_ms(500);
	set_sleep_time();

	display_battery_notify();
}

void show_ble_disconnect_push(bool vibrate)
{
	if(do_not_disturb()){
		return;
	}

	if(vibrate){
		sleep_up(WAKEUP_BLEDISCONNECTED);
		set_motor_ms(1000);
		set_sleep_time();
	}

	display_ble_disconnect_push(); 	
}

void show_antiloss_push(bool wakeup)
{
	if(do_not_disturb()){
		return;
	}
	
	if(wakeup){
		sleep_up(WAKEUP_ANTILOSS);
		set_motor_ms(1000);
		set_sleep_time();
	}

	display_antiloss_notify();
}
