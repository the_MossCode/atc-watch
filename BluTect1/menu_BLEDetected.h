#pragma once

#include "Arduino.h"
#include "menu.h"
#include "class.h"
#include <lvgl.h>
#include "social_distancing.h"
#include "ble.h"

class BLEDetectedScreen: public Screen
{
	public:
	virtual void pre()
	{
		this->temp_device = ble_central_get_scanned_device()->device;

		String message = String("NEARBY DEVICE\nDETECTED:\n");
		if(this->temp_device.local_name_len <= 0){ // no Name, just incase
			memcpy(temp_device.local_name, "(No Name)", sizeof("(No Name)"));
		}

		message += String(this->temp_device.local_name);

		set_gray_screen_style();	
		lv_style_copy(&green_btn_style, &lv_style_plain);
		green_btn_style.body.main_color = LV_COLOR_GREEN;
		green_btn_style.body.grad_color = LV_COLOR_GREEN;
		green_btn_style.body.radius = 2;
		green_btn_style.text.color = BLUTECT_COLOR_MILK;
		green_btn_style.text.font = &lv_font_roboto_22;
		
		lv_style_copy(&red_btn_style, &green_btn_style);
		red_btn_style.body.main_color = LV_COLOR_RED;
		red_btn_style.body.grad_color = LV_COLOR_RED;

		lv_style_copy(&pressed_btn_style, &green_btn_style);
		pressed_btn_style.body.main_color = BLUTECT_COLOR_GRAY;
		pressed_btn_style.body.grad_color = LV_COLOR_BLACK;
		pressed_btn_style.body.border.color = BLUTECT_COLOR_GRAY;
		pressed_btn_style.body.border.width = 1;
		
		msg_label = lv_label_create(lv_scr_act(), NULL);
		lv_label_set_align(msg_label, LV_LABEL_ALIGN_CENTER);
		lv_label_set_text(msg_label, message.c_str());
		lv_obj_align(msg_label, NULL, LV_ALIGN_IN_TOP_MID, 0, 32);
		
		safe_btn = lv_btn_create(lv_scr_act(), NULL);
		lv_btn_set_style(safe_btn, LV_BTN_STYLE_REL, &green_btn_style);
		lv_btn_set_style(safe_btn, LV_BTN_STYLE_PR, &pressed_btn_style);
		lv_obj_align(safe_btn, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 10, -10);
		
		lv_obj_set_event_cb(safe_btn, lv_event_handler);
		
		safe_btn_label = lv_label_create(safe_btn, NULL);
		lv_label_set_align(safe_btn_label, LV_LABEL_ALIGN_LEFT);
		lv_label_set_text(safe_btn_label, "SAFE");
		
		
		danger_btn = lv_btn_create(lv_scr_act(), NULL);
		lv_btn_set_style(danger_btn, LV_BTN_STYLE_REL, &red_btn_style);
		lv_btn_set_style(danger_btn, LV_BTN_STYLE_PR, &pressed_btn_style);
		lv_obj_align(danger_btn, NULL, LV_ALIGN_IN_BOTTOM_RIGHT, -10, -10);
		
		lv_obj_set_event_cb(danger_btn, lv_event_handler);
		
		danger_btn_label = lv_label_create(danger_btn, NULL);
		lv_label_set_align(danger_btn_label, LV_LABEL_ALIGN_CENTER);
		lv_label_set_text(danger_btn_label, "DANGER");
	}

	virtual void main()
	{
	}

	virtual void right()
	{
	}

	virtual void click(touch_data_struct touch_data)
	{
	}
	
	virtual void long_click()
	{
	}

	virtual uint32_t sleepTime()
	{
		return 20000;
	}
	
	virtual void lv_event_class(lv_obj_t * object, lv_event_t event)
	{
		if(object == safe_btn && event == LV_EVENT_SHORT_CLICKED){
			temp_device.list_type = PEER_LIST_TYPE_SAFE;
			sd_alert_add_device_to_safelist(&temp_device);
		}
		else if(object == danger_btn && event == LV_EVENT_SHORT_CLICKED){
			temp_device.list_type = PEER_LIST_TYPE_IGNORE;
			sd_alert_add_device_to_ignore_list(&temp_device);
		}

		sd_alert_ack();
		ble_central_reset_scan();
		set_last_menu();
	}


	private:
	
	lv_obj_t *safe_btn, *danger_btn;
	lv_obj_t *safe_btn_label, *danger_btn_label;
	lv_obj_t *msg_label;
	
	// lv_style_t safe_btn_style, danger_btn_style, msg_label_style;

	ble_peer_device_t temp_device;
};

BLEDetectedScreen bleDetectedScreen;
