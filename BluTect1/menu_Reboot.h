
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"


static const char *reboot_btns[] = {"Yes", "No", ""};

class RebootScreen : public Screen
{
  public:
    virtual void pre()
    {
      lv_obj_t * mbox1 = lv_mbox_create(lv_scr_act(), NULL);
      lv_mbox_set_text(mbox1, "Reboot ?");
      lv_mbox_add_btns(mbox1, reboot_btns);
      lv_obj_set_width(mbox1, 200);
      lv_obj_set_event_cb(mbox1, lv_event_handler);
      lv_obj_align(mbox1, NULL, LV_ALIGN_CENTER, 0, 0);
    }

    virtual void main()
    {

    }
    virtual void right()
    {
      set_last_menu();
    }

    virtual void click(touch_data_struct touch_data)
    {

    }

    virtual void pre_display()// if this is set, the screen gets not cleared
    {
    }

    virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
    {
      if (event == LV_EVENT_VALUE_CHANGED) {
        if ("Yes" == lv_mbox_get_active_btn_text(object))
          set_reboot();
        else if ("No" == lv_mbox_get_active_btn_text(object))
          set_last_menu();
      }      
    }
};

RebootScreen rebootScreen;
