
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

#include "antiloss.h"


class AntilossPushScreen : public Screen
{
	public:
	virtual void pre()
	{
		ble_peer_device_t* tempDevice;
		tempDevice = antiloss_get_device();

		set_gray_screen_style();
		lv_style_copy(&generic_btn_style, &lv_style_plain);
		generic_btn_style.body.grad_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.main_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.radius = 2;
		generic_btn_style.text.color = BLUTECT_COLOR_MILK;
		generic_btn_style.text.font = &lv_font_roboto_22;

		lv_style_copy(&pressed_btn_style, &generic_btn_style);
		pressed_btn_style.body.grad_color = LV_COLOR_BLACK;
		pressed_btn_style.body.border.color = LV_COLOR_GRAY;
		pressed_btn_style.body.border.width = 1;
		
		String label_msg = "TAG LOST:\n";
		label_msg += String(tempDevice->local_name);
		
		msg_label = lv_label_create(lv_scr_act(), NULL);
		lv_label_set_align(msg_label, LV_LABEL_ALIGN_CENTER);
		lv_label_set_text(msg_label, label_msg.c_str());
		lv_obj_align(msg_label, NULL, LV_ALIGN_IN_TOP_MID, 0, 32);
		
		forget_btn = lv_btn_create(lv_scr_act(), NULL);
		lv_btn_set_style(forget_btn, LV_BTN_STATE_REL, &generic_btn_style);
		lv_btn_set_style(forget_btn, LV_BTN_STYLE_PR, &pressed_btn_style);
		lv_obj_align(forget_btn, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 10, -10);
		
		lv_obj_set_event_cb(forget_btn, lv_event_handler);
		
		forget_btn_label = lv_label_create(forget_btn, NULL);
		lv_label_set_align(forget_btn_label, LV_LABEL_ALIGN_LEFT);
		lv_label_set_text(forget_btn_label, "FORGET");
		
		snooze_btn = lv_btn_create(lv_scr_act(), NULL);
		lv_btn_set_style(snooze_btn, LV_BTN_STATE_REL, &generic_btn_style);
		lv_btn_set_style(snooze_btn, LV_BTN_STATE_PR, &pressed_btn_style);
		lv_obj_align(snooze_btn, NULL, LV_ALIGN_IN_BOTTOM_RIGHT, -10, -10);
		
		lv_obj_set_event_cb(snooze_btn, lv_event_handler);
		
		snooze_btn_label = lv_label_create(snooze_btn, NULL);
		lv_label_set_align(snooze_btn_label, LV_LABEL_ALIGN_CENTER);
		lv_label_set_text(snooze_btn_label, "SNOOZE");
	}

	virtual void main()
	{
	}

	virtual void right()
	{

	}

	virtual void post()
	{
		
	}

	virtual uint32_t sleepTime()
	{
		return 20000;
	}
	
	virtual void lv_event_class(lv_obj_t * object, lv_event_t event)
	{
		if(object == forget_btn && event == LV_EVENT_SHORT_CLICKED){
			antiloss_set_notify(ALOSS_NOTIFY_DISABLE);
			antiloss_set_state(ANTILOSS_STATE_DEL_DEVICE);
			display_home();
		}
		else if(object == snooze_btn && event == LV_EVENT_SHORT_CLICKED){
			antiloss_set_notify(ALOSS_NOTIFY_DISABLE);
			if(antiloss_get_state() != ANTILOSS_STATE_CONNECTED){
				antiloss_set_state(ANTILOSS_STATE_LOST);
			}
			
			ble_write("AT+TAG:2");
			display_home();
		}
	}


	private:
	lv_obj_t *forget_btn, *snooze_btn;
	lv_obj_t *forget_btn_label, *snooze_btn_label;
	lv_obj_t *msg_label;
	
	// lv_style_t forget_btn_style, snooze_btn_style, msg_label_style;
};

AntilossPushScreen antilossPushScreen;
