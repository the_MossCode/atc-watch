#ifndef BLE_DETECTED_PUSH_H
#define BLE_DETECTED_PUSH_H

#define SDD_BUFFER_SIZE          5
#define SDD_MAX_COUNT           10

#include <Arduino.h>
#include "ble_peer_common.h"
#include "debug_utils.h"
#include "flash.h"

enum sd_state{
	OBSERVER_STATE_IDLE,
	OBSERVER_STATE_NOTIFY,
	OBSERVER_STATE_DISABLE,
	OBSERVER_STATE_DISABLED
};

void social_distancing_init();
void sd_alert_add_device_to_safelist(ble_peer_device_t *peer_device);
void sd_alert_add_device_to_ignore_list(ble_peer_device_t *peer_device);

bool sd_alert_is_device_in_safelist(ble_peer_device_t *peer_device);
bool sd_alert_is_device_in_ignore_list(ble_peer_device_t *peer_device);

void sd_alert_get_local_name(char *localName, byte index);

void sd_alert_enable();
void sd_alert_disable();

uint8_t sd_alert_get_state();
void sd_alert_ack();
// void set_sd_alert_state(byte state);

void sd_alert_loop();

#if (SAVE_TO_FLASH)
bool open_sdd_flash_file(spiffs_flags flags);
bool close_sdd_flash_file();

bool get_sdd_mac_address_from_flash(byte index, byte *addr);
bool get_sdd_name_from_flash(byte index, char *name);
bool get_sdd_head_from_flash();

bool save_sdd_to_flash(ble_peer_device_t *sdd);
bool init_sdd_from_flash();
#endif

#endif //BLE_DETECTED_PUSH_H