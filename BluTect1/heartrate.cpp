
#include "heartrate.h"
#include "pinout.h"
#include "i2c.h"
#include "inputoutput.h"
#include "sleep.h"
#include "HRS3300lib.h"

#include "ble.h"
#include "flash.h"
#include "time.h"

#include "accl.h"

#define HR_LOG_F_NAME		"hr_data.hr"

bool heartrate_enable = false;

byte last_heartrate_ms;
byte last_heartrate = 0;

byte hr_buf[HR_BUFFER_SIZE];
byte hr_buf_head = 0, hr_buf_count = 0;

//timed heartrate stuff
bool timed_heart_rates = false;
bool has_good_heartrate = false;
int hr_answers;
bool disabled_hr_allready = false;
byte heartrate_measurement_frequency = 10;

void init_hrs3300()
{
	pinMode(HRS3300_TEST, INPUT);
	HRS3300_begin(user_i2c_read, user_i2c_write); //set the i2c read and write function so it can be a user defined i2c hardware see i2c.h
	heartrate_enable = true;
	end_hrs3300();
	
	set_timed_heartrate_interval(5);

	memset(hr_buf, 0x00, HR_BUFFER_SIZE);

	#if SAVE_TO_FLASH
	init_hr_data_from_flash();
	#endif
}

void start_hrs3300()
{
	if (!heartrate_enable)
	{
		HRS3300_enable();
		heartrate_enable = true;
	}
}

void end_hrs3300()
{
	if (heartrate_enable)
	{
		heartrate_enable = false;
		HRS3300_disable();
	}
}

byte get_heartrate()
{
	byte hr = last_heartrate_ms;
	switch (hr)
	{
		case 0:
		break;
		case 255:
		break;
		case 254: //No Touch
		break;
		case 253: //Please wait
		break;
		default:
		last_heartrate = hr;
		break;
	}
	return hr;
}

byte get_last_heartrate()
{
	if(hr_buf_head == 0){
		return get_last_heartrate_data_by_index(HR_BUFFER_SIZE-1);
	}
	else{
		return get_last_heartrate_data_by_index(hr_buf_head-1);
	}
}

byte get_last_heartrate_data_by_index(byte index)
{
	return (index < HR_BUFFER_SIZE) ? hr_buf[index] : 0; 
}

void get_heartrate_ms()
{
		if (heartrate_enable)
		{
			last_heartrate_ms = HRS3300_getHR();
		}
}

void check_timed_heartrate(int minutes)
{
	// if (timed_heart_rates || nwa_is_active())
	// {
		if (minutes == 0 || ((minutes % heartrate_measurement_frequency) == 0))
		{
			if(!timed_heart_rates && !nwa_is_active()){
				return;
			}

			if (!has_good_heartrate)
			{
				disabled_hr_allready = false;
				start_hrs3300();
				byte hr = get_heartrate();
				if (hr > 0 && hr < 253){
					if(++hr_answers >=10){
						has_good_heartrate = true;
						add_hr_to_buffer(hr);
						ble_write("AT+HR:"+String(hr));
						
						if(nwa_is_active()){
							nwa_buf_add_hr(hr);
						}
					}
				}
				else if (hr == 254)
				{
					hr_answers++;
					if (hr_answers >= 10)
					{
						has_good_heartrate = true;
					}
				}
			}
			else
			{
				end_hrs3300();
			}
		}
		else
		{
			if (!disabled_hr_allready)
			{
				disabled_hr_allready = true;
				end_hrs3300();
				hr_answers = 0;
				has_good_heartrate = false;
			}
		}
	// }
}

void add_hr_to_buffer(byte hr)
{
	hr_buf[hr_buf_head++] = hr;

	if(hr_buf_head >= HR_BUFFER_SIZE){
		hr_buf_head = 0;
	}

	#if SAVE_TO_FLASH
	if(!save_hr_data_to_flash(hr)){
		BLE_DEBUG_PRINT("ERR_SAVE_HR_DATA");
		return;
	}
	#endif

	if(++hr_buf_count >= HR_BUFFER_SIZE){
		hr_buf_count = HR_BUFFER_SIZE;
	}
}

byte get_hr_buffer_count()
{
	return hr_buf_count;
}

void set_timed_heartrate_interval(byte minutes)
{
	heartrate_measurement_frequency = minutes;
}

void heartrate_enable_monitoring(uint8_t enable)
{
	// Set interval to 15 minutes only when not in wake up alarm window
	if(!nwa_is_active()){
		set_timed_heartrate_interval(15);
	}

	if(enable){
		timed_heart_rates = true;
	}
	else{
		timed_heart_rates = false;
	}
}

uint8_t heartrate_get_monitoring_status()
{
	return timed_heart_rates;
}

byte get_max_hr()
{
	byte temp_hr = 0;
	for(int i=0; i<HR_BUFFER_SIZE; ++i){
		byte log_hr = get_last_heartrate_data_by_index(i);
		if(log_hr > temp_hr && log_hr != 255){
			temp_hr = log_hr;
		}
	}
	
	return temp_hr;
}

byte get_min_hr()
{
	byte temp_hr = 250;
	for(int i=0; i<HR_BUFFER_SIZE; ++i){
		byte log_hr = get_last_heartrate_data_by_index(i);
		if(log_hr < temp_hr){
			temp_hr = log_hr;
		}
	}
	
	return temp_hr;
}

byte get_average_hr()
{
	unsigned int sum = 0;
	
	for(int i=0; i<HR_BUFFER_SIZE; ++i){
		byte temp_hr = get_last_heartrate_data_by_index(i);
		sum += temp_hr;
	}
	
	return  (sum / HR_BUFFER_SIZE);
}

#if SAVE_TO_FLASH
boolean read_hr_data_from_flash(byte *data)
{
	spiffs_file hr_file = flash_open_file(HR_LOG_F_NAME, SPIFFS_RDONLY);
	if(hr_file < 0){
		BLE_DEBUG_PRINT("ERR_OPEN_HR:" + String(flash_get_last_error()));
		return false;
	}

	bool is_op_success = true;
	if(flash_read_file(&hr_file, (char*)&hr_buf_head, 1) < 0){
		BLE_DEBUG_PRINT("ERR_RD_H_HR");
		is_op_success = false;
	}

	if(is_op_success){
		if(flash_read_file(&hr_file, (char*)data, HR_BUFFER_SIZE) < 0){
			BLE_DEBUG_PRINT("ERR_RD_HR");
			is_op_success = false;
		}
	}


	flash_close_file(&hr_file);

	return is_op_success;
}

boolean save_hr_data_to_flash(byte hr)
{
	spiffs_file hr_file = flash_open_file(HR_LOG_F_NAME, SPIFFS_RDWR);
	if(hr_file < 0){
		BLE_DEBUG_PRINT("ERR_OPEN_WR_HR:" + String(flash_get_last_error()));
		return false;
	}	

	bool is_op_success = true;
	if(flash_seek_file(&hr_file, 1) < 0){// Address 0 is the hr_buf_head
		BLE_DEBUG_PRINT("ERR_SEEK_HR" + String(flash_get_last_error()));
		is_op_success = false;		
	}

	if(is_op_success){
		if(flash_write_file(&hr_file, (const char*)hr_buf, HR_BUFFER_SIZE) < 0){
			BLE_DEBUG_PRINT("ERR_WR_HR");
			is_op_success = false;		
		}
	}

	if(is_op_success){
		if(flash_seek_file(&hr_file, 0) < 0){
			BLE_DEBUG_PRINT("ERR_SEEK_H_HR");
			is_op_success = false;		
		}
	}

	if(is_op_success){
		if(flash_write_file(&hr_file, (const char*)&hr_buf_head, 1) < 0){
			BLE_DEBUG_PRINT("ERR_WR_HR");
			is_op_success = false;		
		}
	}

	flash_close_file(&hr_file);
	return is_op_success;
}

static boolean init_hr_data_from_flash()
{
	if (!read_hr_data_from_flash(hr_buf))
	{
		spiffs_file hr_file = flash_open_file(HR_LOG_F_NAME, SPIFFS_RDWR | SPIFFS_CREAT);
		if (hr_file < 0)
		{
			BLE_DEBUG_PRINT("ERR_OPEN_WR_HR:" + String(flash_get_last_error()));
			return false;
		}

		if(flash_write_file(&hr_file, (const char*)&hr_buf_head, 1) < 0){
			flash_close_file(&hr_file);
			return false;
		}

		if(flash_write_file(&hr_file, (const char*)hr_buf, HR_BUFFER_SIZE) < 0){
			flash_close_file(&hr_file);
			return false;
		}

		flash_close_file(&hr_file);
	}

	return true;
}
#endif // Save to flash