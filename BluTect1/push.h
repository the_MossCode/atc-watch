
#pragma once

#include "Arduino.h"

#define DND_SOURCE_NONE     0
#define DND_SOURCE_USER     1
#define DND_SOURCE_ALARM    2

void init_push();
void show_push(String pushMSG);
void show_push(String pushMSG, bool vibrate);

bool do_not_disturb();
void enable_do_not_disturb(bool enable, byte source);

void show_http(String httpMSG);
String get_http_msg(int returnLength=0);
String get_push_msg(int returnLength=0);

void show_ble_detected(bool wake_up);
void show_alarm_push(bool vibrate=true);
void show_low_battery_push();
void show_ble_disconnect_push(bool vibrate=true);

void show_antiloss_push(bool wakeup = true);
