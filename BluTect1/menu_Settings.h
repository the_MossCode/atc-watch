#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

const char *settings_m_map[] = {
	"Time", "Date", "\n",
	"Brightness", "Reboot", "\n",
	"Shut Down", "Bootloader", "",
};

class SettingsScreen : public Screen
{
	public:
	virtual void pre()
	{
		set_gray_screen_style();
		lv_style_copy(&settings_style, lv_obj_get_style(lv_scr_act()));
		settings_style.body.main_color = BLUTECT_COLOR_GRAY;
		settings_style.body.main_color = BLUTECT_COLOR_GRAY;
		settings_style.body.border.color = LV_COLOR_GRAY;
		settings_style.body.border.width = 1;
		settings_style.body.radius = 2;

		lv_style_copy(&pressed_btn_style, &settings_style);
		pressed_btn_style.body.grad_color = LV_COLOR_BLACK;
		pressed_btn_style.body.border.part = 5;
		
		settings_m = lv_btnm_create(lv_scr_act(), NULL);

		lv_btnm_set_map(settings_m, settings_m_map);
		lv_obj_set_size(settings_m, 240, 240);
		lv_obj_align(settings_m, NULL, LV_ALIGN_CENTER, 0, 0);
		lv_obj_set_event_cb(settings_m, lv_event_handler);

		lv_btnm_set_style(settings_m, LV_BTNM_STYLE_BG, &settings_style);
		lv_btnm_set_style(settings_m, LV_BTNM_STYLE_BTN_REL, &settings_style);
		lv_btnm_set_style(settings_m, LV_BTNM_STYLE_BTN_PR, &pressed_btn_style);
		BLE_DEBUG_PRINT("Settings menu init complete");
	}

	virtual void main()
	{
	}

	virtual void up()
	{

	}

	virtual void down()
	{

	}

	virtual void right()
	{
		display_home();
	}

	virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
	{
		if (event == LV_EVENT_VALUE_CHANGED)
		{
			switch(lv_btnm_get_pressed_btn(settings_m)){
				case 0:{// Time
					change_screen(&settingsTimeScreen);
					break;
				}
				case 1:{// Date
					change_screen(&settingsDateScreen);
					break;
				}
				case 2:{// Brightness
					change_screen(&settingsBrightnessScreen);
					break;
				}
				case 3:{// Reboot
					change_screen(&rebootScreen);
					break;
				}
				case 4:{// Shutdown
					change_screen(&offScreen);
					break;
				}
				case 5:{// Bootloader
					change_screen(&updateScreen);
					break;
				}
				default:{
					break;
				}
			}
		}
	}

	private:
	lv_obj_t *settings_m;
	lv_style_t settings_style;
};

SettingsScreen settingsScreen;
