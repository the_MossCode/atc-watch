#include "ble_central.h"
#include "antiloss.h"

enum{
    scan_state_init, scan_state_scanning, scan_state_stop, scan_state_error
}scan_states;

static void scan_loop();

BLECentralRole bleCentral = BLECentralRole();
uint8_t ble_central_state = ble_central_state_init;
uint8_t scan_state = scan_state_init;
scanned_device_t scanned_device;


void ble_central_init()
{
 	bleCentral.setScanEventCB(ble_central_scan_callback);
	bleCentral.setConnectedEventCB(ble_central_connected_cb);
	bleCentral.setDisconnectedEventCB(ble_central_disconnected_cb);
	bleCentral.setTimeoutEventCB(ble_central_timeout_cb);
	bleCentral.setAttrDiscoveryCompleteCB(ble_central_att_discovery_complete_cb);   

    if(bleCentral.begin()){
        ble_central_state = ble_central_state_idle;

        uint32_t res = bleCentral.startScan();
        if(res == NRF_SUCCESS || res == NRF_ERROR_INVALID_STATE){
            ble_central_state = ble_central_state_scan;
            scan_state = scan_state_scanning;
        }
        else{
            scan_state = scan_state_error;
        }
    }
}

void ble_central_loop(uint32_t *buf, uint16_t *buf_len)
{
    bleCentral.poll(buf, buf_len);

    switch(ble_central_state){
        case ble_central_state_init:{
            if(bleCentral.begin()){
                ble_central_state = ble_central_state_idle;
            }
        }
        case ble_central_state_idle:{
            break;
        }
        case ble_central_state_scan:{
            scan_loop();
        }
        default:{
            break;
        }
    }
}

void ble_central_add_remote_att(BLERemoteAttribute &att)
{
    bleCentral.addRemoteAttribute(att);
}

uint32_t ble_central_connect(ble_peer_device_t &device)
{
    return bleCentral.connect(&device.peer_address);
}

uint32_t ble_central_disconnect(ble_peer_device_t *device)
{
    return bleCentral.disconnect();
}

uint32_t ble_central_cancel_connect()
{
    return bleCentral.cancelConnection();
}

void ble_central_start_scan()
{
    ble_central_state = ble_central_state_scan;
    scan_state = scan_state_init;
}

void ble_central_stop_scan()
{
    if(ble_central_state == ble_central_state_scan){
        scan_state = scan_state_stop;
    }
}

bool ble_central_is_new_scanned_device()
{
    return scanned_device.is_new_device;
}

scanned_device_t *ble_central_get_scanned_device()
{
    return &scanned_device;
}

void ble_central_reset_scan()
{
    memset(&scanned_device, 0x00, sizeof(scanned_device));
}

uint8_t ble_central_get_state()
{
    return ble_central_state;
}

void ble_central_scan_callback(ble_gap_addr_t* gap_addr, uint8_t* data, uint8_t dataLen, int8_t rssi)
{
	if(scanned_device.is_new_device){
		return;
	}

	byte dataIndex = 0, structDataLen = 0;
	scanned_device.device.local_name_len = 0;
	memset(scanned_device.device.local_name, 0x00, LOCAL_NAME_MAX_SIZE);

	while (dataIndex < dataLen){
		structDataLen = data[dataIndex];
		dataIndex += 1;

		byte advType = data[dataIndex];

		switch (advType){
			case BLE_GAP_AD_TYPE_SHORT_LOCAL_NAME:{
				dataIndex += 1;
				scanned_device.device.local_name_len = structDataLen - 1;

				for (int i = 0; i < (structDataLen - 1); ++i)
				{
					scanned_device.device.local_name[i] = data[dataIndex];
					dataIndex++;
				}

				break;
			}
			case BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME:{
				dataIndex += 1;
				scanned_device.device.local_name_len = structDataLen - 1;

				for (int i = 0; i < (structDataLen - 1); ++i)
				{
					scanned_device.device.local_name[i] = data[dataIndex];
					dataIndex++;
				}

				break;
			}
			default:{
				dataIndex += structDataLen;
				break;
			}
		} 
	}

	scanned_device.device.rssi = rssi;
	scanned_device.device.peer_address = *gap_addr;
	if(scanned_device.device.local_name_len > 0){
		scanned_device.is_new_device = true;
	}
}

void ble_central_connected_cb(ble_gap_addr_t* peerAddress)
{
    antiloss_set_state(ANTILOSS_STATE_CONNECTED);
}

void ble_central_disconnected_cb(uint8_t reason)
{
    antiloss_set_state(ANTILOSS_STATE_DISCONNECTED);
}

void ble_central_timeout_cb(uint8_t source)
{
    // todo
    ;;
}

void ble_central_att_discovery_complete_cb()
{
    antiloss_gatt_attribute_discover_callback();
    BLE_DEBUG_PRINT("DC_1");
}

static void scan_loop()
{
    switch (scan_state) {
        case scan_state_init:{
            uint32_t res = bleCentral.startScan();
            if(res == NRF_SUCCESS){
                scan_state = scan_state_scanning;
            }
            else if (res == NRF_ERROR_INVALID_STATE){
                scan_state = scan_state_scanning;
            }
            else{
                scan_state = scan_state_error;
            }
            break;
        }
        case scan_state_scanning:{
            break;
        }
        case scan_state_stop:{
            uint32_t res = bleCentral.stopScan();
            if(res == NRF_SUCCESS || res == NRF_ERROR_INVALID_STATE){
                scan_state = scan_state_init;
                ble_central_state = ble_central_state_idle;

                scanned_device.is_new_device = false;
            }
            break;
        }
        case scan_state_error:{
            // todo attempt recovery
            break;
        }
        default:{
            break;
        }
    }
}