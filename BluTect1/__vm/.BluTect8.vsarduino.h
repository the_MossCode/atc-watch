/* 
	Editor: https://www.visualmicro.com/
			visual micro and the arduino ide ignore this code during compilation. this code is automatically maintained by visualmicro, manual changes to this file will be overwritten
			the contents of the _vm sub folder can be deleted prior to publishing a project
			all non-arduino files created by visual micro and all visual studio project or solution files can be freely deleted and are not required to compile a sketch (do not delete your own code!).
			note: debugger breakpoints are stored in '.sln' or '.asln' files, knowledge of last uploaded breakpoints is stored in the upload.vmps.xml file. Both files are required to continue a previous debug session without needing to compile and upload again
	
	Hardware: DaFit Watch Bootloader 23, Platform=nRF5, Package=sandeepmistry
*/

#define ARDUINO 10809
#define ARDUINO_MAIN
#define F_CPU 16000000
#define printf iprintf
#define __NRF5__
#define CONFIG_NFCT_PINS_AS_GPIOS
#define USE_LFRC
#define NRF52
#define S132
#define NRF51_S132
#define F_CPU 16000000
#define ARDUINO 10809
#define ARDUINO_DaPinout
#define ARDUINO_ARCH_NRF5
#include "pins_arduino.h" 
#include "variant.h" 
#include "arduino.h"
#include "BluTect8.ino"
