
#ifndef HEARTRATE_H
#define HEARTRATE_H

#include "Arduino.h"
#include "flash.h"

#include "debug_utils.h"

#define HR_BUFFER_SIZE		120

void init_hrs3300();
void start_hrs3300();
void end_hrs3300();
byte get_heartrate();
byte get_last_heartrate();
byte get_last_heartrate_data_by_index(byte index);
void get_heartrate_ms();
void check_timed_heartrate(int minutes);

void add_hr_to_buffer(byte hr);
byte get_hr_buffer_count();

void set_timed_heartrate_interval(byte minutes);
void heartrate_enable_monitoring(uint8_t enable);
uint8_t heartrate_get_monitoring_status();

//
byte get_max_hr();
byte get_min_hr();
byte get_average_hr();

// Flash Operations
#if SAVE_TO_FLASH
boolean read_hr_data_from_flash(byte *data);
boolean save_hr_data_to_flash(byte hr);
static boolean init_hr_data_from_flash();
#endif

#if DEBUG
//String debug_hr_flash();
#endif

#endif