#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"
#include <lvgl.h>

struct app_struct
{
  const char *_title;
  const lv_img_dsc_t *_symbol;
  Screen *_screen;
};

class AppScreen : public Screen
{
public:
  AppScreen(int menuPosition, int maxApps, app_struct *app1, app_struct *app2, app_struct *app3 , app_struct* app4)
  {
    _menuPosition = menuPosition;
    _maxApps = maxApps;
    _app1 = app1;
    _app2 = app2;
    _app3 = app3;
    _app4 = app4;
  }

  virtual void pre()
  {
    slider = lv_slider_create(lv_scr_act(), NULL);
    lv_obj_set_size(slider, 15, 100);
    lv_slider_set_range(slider, 1, _maxApps);
    lv_slider_set_value(slider, (_maxApps - _menuPosition) + 1, false);
    lv_obj_align(slider, NULL, LV_ALIGN_IN_RIGHT_MID, -4, 0);
    lv_obj_set_click(slider, false);
    lv_obj_set_click(lv_page_get_scrl(slider), false);

    label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text_fmt(label, "%i/%i", _menuPosition, _maxApps);
    lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    set_gray_screen_style();
    lv_style_copy(&global_screen_style, lv_obj_get_style(lv_scr_act()));

    app1_cont = lv_cont_create(lv_scr_act(), NULL);
    lv_obj_set_size(app1_cont, 110, 110);
    lv_obj_align(app1_cont, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 20);
    lv_cont_set_layout(app1_cont, LV_LAYOUT_COL_M);
    lv_obj_set_event_cb(app1_cont, lv_event_handler);
    lv_cont_set_style(app1_cont, LV_CONT_STYLE_MAIN, &global_screen_style);

    button_image1 = lv_img_create(app1_cont, NULL);
    lv_img_set_src(button_image1, _app1->_symbol);

    image_label1 = lv_label_create(app1_cont, NULL);
    lv_label_set_text(image_label1, _app1->_title);

    if (_app2 != NULL)
    {
      app2_cont = lv_cont_create(lv_scr_act(), NULL);
      lv_obj_set_size(app2_cont, 110, 110);
      lv_obj_align(app2_cont, NULL, LV_ALIGN_IN_TOP_LEFT, 110, 20);
      lv_cont_set_layout(app2_cont, LV_LAYOUT_COL_M);
      lv_obj_set_event_cb(app2_cont, lv_event_handler);
      lv_cont_set_style(app2_cont, LV_CONT_STYLE_MAIN, &global_screen_style);

      button_image2 = lv_img_create(app2_cont, NULL);
      lv_img_set_src(button_image2, _app2->_symbol);

      image_label2 = lv_label_create(app2_cont, NULL);
      lv_label_set_text(image_label2, _app2->_title);
    }

    if (_app3 != NULL)
    {
      app3_cont = lv_cont_create(lv_scr_act(), NULL);
      lv_obj_set_size(app3_cont, 110, 110);
      lv_obj_align(app3_cont, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
      lv_cont_set_layout(app3_cont, LV_LAYOUT_COL_M);
      lv_obj_set_event_cb(app3_cont, lv_event_handler);
      lv_cont_set_style(app3_cont, LV_CONT_STYLE_MAIN, &global_screen_style);

      button_image3 = lv_img_create(app3_cont, NULL);
      lv_img_set_src(button_image3, _app3->_symbol);

      image_label3 = lv_label_create(app3_cont, NULL);
      lv_label_set_text(image_label3, _app3->_title);
    }

    if (_app4 != NULL)
    {
      app4_cont = lv_cont_create(lv_scr_act(), NULL);
      lv_obj_set_size(app4_cont, 110, 110);
      lv_obj_align(app4_cont, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 110, 0);
      lv_cont_set_layout(app4_cont, LV_LAYOUT_COL_M);
      lv_obj_set_event_cb(app4_cont, lv_event_handler);
      lv_cont_set_style(app4_cont, LV_CONT_STYLE_MAIN, &global_screen_style);

      button_image4 = lv_img_create(app4_cont, NULL);
      lv_img_set_src(button_image4, _app4->_symbol);

      image_label4 = lv_label_create(app4_cont, NULL);
      lv_label_set_text(image_label4, _app4->_title);
    }
  }

  virtual void main()
  {
  }

  virtual void up()
  {
    inc_vars_menu();
  }

  virtual void down()
  {
    dec_vars_menu();
  }

  virtual void right()
  {
    display_home();
  }

  virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
  {
    if (event == LV_EVENT_SHORT_CLICKED)
    {
      if (object == app1_cont)
      {
        change_screen(_app1->_screen);
      }
      else if (object == app2_cont)
      {
        change_screen(_app2->_screen);
      }
      else if (object == app3_cont)
      {
        change_screen(_app3->_screen);
      }
      else if(object == app4_cont){
        change_screen(_app4->_screen);
      }
    }
  }

private:
  app_struct *_app1;
  app_struct *_app2;
  app_struct *_app3;
  app_struct *_app4;
  lv_obj_t *button_image1, *button_image2, *button_image3 , *button_image4, *slider, *label;
  lv_obj_t *image_label1, *image_label2, *image_label3, *image_label4;
  lv_obj_t *app1_cont, *app2_cont, *app3_cont, *app4_cont;
  
  uint32_t _menuPosition, _maxApps;
};
