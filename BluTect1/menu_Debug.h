#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "sleep.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"
#include "backlight.h"

#include "flash.h"
#include "social_distancing.h"

#include "pedometer.h"

#if DEBUG

class DebugScreen : public Screen
{
  public:
    virtual void pre()
    {
      label = lv_label_create(lv_scr_act(), NULL);
      lv_label_set_text(label, "Debug");
      lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);

      main_list = lv_list_create(lv_scr_act(), NULL);
		  lv_list_set_anim_time(main_list, 0);
		  lv_list_set_sb_mode(main_list, LV_SB_MODE_ON);
		  lv_list_set_layout(main_list, LV_LAYOUT_COL_L);
		  lv_obj_set_width(main_list, 240);
		  lv_obj_set_height(main_list, 200);
      lv_obj_align(main_list, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 40);
		
		  lv_list_set_style(main_list, LV_LIST_STYLE_BG, &global_screen_style);
		  lv_list_set_style(main_list, LV_LIST_STYLE_BTN_PR, &global_screen_style);
		  lv_list_set_style(main_list, LV_LIST_STYLE_BTN_REL, &global_screen_style); 

      btn_millis = lv_list_add_btn(main_list, NULL, "Millis: ");     
      label_millis = lv_list_get_btn_label(btn_millis);

      btn_uptime = lv_list_add_btn(main_list, NULL, "Uptime: ");     
      label_uptime = lv_list_get_btn_label(btn_uptime);

      btn_reset = lv_list_add_btn(main_list, NULL, "Reset: ");     
      label_reset = lv_list_get_btn_label(btn_reset);
      lv_label_set_text_fmt(label_reset, "Reset: %02d", NRF_POWER->RESETREAS);

      btn_peipheral_state = lv_list_add_btn(main_list, NULL, "BLE: ");     
      label_peipheral_state = lv_list_get_btn_label(btn_peipheral_state);

      btn_flash = lv_list_add_btn(main_list, NULL, "Flash ID: ");     
      label_flash = lv_list_get_btn_label(btn_flash);
      lv_label_set_text_fmt(label_flash, "Flash ID: %06x", flash_read_id());

      btn_flash_free_mem = lv_list_add_btn(main_list, NULL, "Free Mem: ");
      label_flash_free_mem = lv_list_get_btn_label(btn_flash_free_mem);

      btn_ble_last_op = lv_list_add_btn(main_list, NULL, "AcclID: ");     
      label_ble_last_op = lv_list_get_btn_label(btn_ble_last_op);
      accl_data_struct temp_accl = get_accl_data();
      lv_label_set_text_fmt(label_ble_last_op, "AcclID: %d", temp_accl.device_id);

      btn_x_accl = lv_list_add_btn(main_list, NULL, "AcclX:");
      label_x_accl = lv_list_get_btn_label(btn_x_accl);

      btn_y_accl = lv_list_add_btn(main_list, NULL, "AcclY:");
      label_y_accl = lv_list_get_btn_label(btn_y_accl);

      btn_z_accl = lv_list_add_btn(main_list, NULL, "AcclZ:");
      label_z_accl = lv_list_get_btn_label(btn_z_accl);

      btn_pedo_t = lv_list_add_btn(main_list, NULL, "Pedometer:-");

      btn_pedo_threshold = lv_list_add_btn(main_list, NULL, "Thr:");
      label_pedo_threshold = lv_list_get_btn_label(btn_pedo_threshold);

      btn_pedo_max = lv_list_add_btn(main_list, NULL, "Max:");
      label_pedo_max = lv_list_get_btn_label(btn_pedo_max);
      
      btn_pedo_min = lv_list_add_btn(main_list, NULL, "Min:");
      label_pedo_min = lv_list_get_btn_label(btn_pedo_min);

      btn_pedo_diff = lv_list_add_btn(main_list, NULL, "Diff:");
      label_pedo_diff = lv_list_get_btn_label(btn_pedo_diff);



      s32_t free_mem;
      u32_t used_mem, total_mem;
      free_mem = flash_get_free_memory(&used_mem, &total_mem);
      if(free_mem < 0){
        lv_label_set_text(label_flash_free_mem, "Flash Err");
      }
      else{
        lv_label_set_text_fmt(label_flash_free_mem, "%.1f/%.1fkB Used", (float)(used_mem/1024), (float)(total_mem/1024));
      }

    }

    virtual void main()
    {
      long days = 0;
      long hours = 0;
      long mins = 0;
      long secs = 0;
      secs = millis() / 1000;
      mins = secs / 60;
      hours = mins / 60;
      days = hours / 24;
      secs = secs - (mins * 60);
      mins = mins - (hours * 60);
      hours = hours - (days * 24);

      char time_string[14];
      sprintf(time_string, "%i %02i:%02i:%02i", days, hours, mins, secs);

      //ble_debug_get_vars(&ble_op, &ble_err, ble_custom_data);
      //lv_label_set_text_fmt(label_ble_last_op, "BLE Op: %d | %04x", ble_op, ble_err);
      //if(get_antiloss_state() == ANTILOSS_STATE_CONNECTED){
      //  lv_label_set_text(label_peipheral_state, "BLE: Connected");
      //}

      lv_label_set_text_fmt(label_millis, "millis: %d", millis());
      lv_label_set_text_fmt(label_uptime, "Uptime: %s", time_string);

      accl_data_struct temp_accl = get_accl_data();

      lv_label_set_text_fmt(label_x_accl, "AcclX: %d", temp_accl.x);
      lv_label_set_text_fmt(label_y_accl, "AcclY: %d", temp_accl.y);
      lv_label_set_text_fmt(label_z_accl, "AcclZ: %d", temp_accl.z);

      lv_label_set_text_fmt(label_pedo_threshold, "Thr: %d", pedometer_debug_get_threshold());
      lv_label_set_text_fmt(label_pedo_min, "Min: %d", pedometer_debug_get_min());
      lv_label_set_text_fmt(label_pedo_max, "Max: %d", pedometer_debug_get_max());
      lv_label_set_text_fmt(label_pedo_diff, "Diff: %d", pedometer_debug_get_diff());
    }

    virtual void right()
    {
      set_last_menu();
    }

    virtual void up()
    {
      lv_list_up(main_list);
    }

    virtual void down()
    {
      lv_list_down(main_list);
    }
    
  private:
    lv_obj_t *label;
    lv_obj_t *btn_millis, *btn_uptime, *btn_reset, *btn_peipheral_state, *btn_ble_last_op ,*btn_flash, *btn_flash_free_mem;
    lv_obj_t *label_millis, *label_uptime, *label_reset, *label_peipheral_state, *label_ble_last_op ,*label_flash, *label_flash_free_mem;

    lv_obj_t *btn_x_accl, *btn_y_accl, *btn_z_accl;
    lv_obj_t *label_x_accl, *label_y_accl, *label_z_accl;

    lv_obj_t *btn_pedo_t, *btn_pedo_threshold, *btn_pedo_max, *btn_pedo_min, *btn_pedo_diff;
    lv_obj_t *label_pedo_threshold, *label_pedo_max, *label_pedo_min, *label_pedo_diff;

    lv_obj_t *main_list;
    
    int ble_op;
    uint32_t ble_err;
    uint8_t *ble_custom_data;

    char * wakeup_reason[11] = {"Unset", "Push", "Connect", "Disconnect", "Charged", "Charge", "Button", "Touch", "Accl", "AcclINT","HTTP"};

};

DebugScreen debugScreen;

#endif //DEBUG
