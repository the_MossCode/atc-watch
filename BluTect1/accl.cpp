#include "accl.h"
#include "Arduino.h"
#include "i2c.h"
#include "pinout.h"
#include "bma423.h"
//https://github.com/BoschSensortec/BMA423-Sensor-API
//Many Thanks to Daniel Thompson(https://github.com/daniel-thompson/wasp-os) to giving the Hint to use a modified BMA423 Library
#include "watchdog.h"
#include "inputoutput.h"
#include "sleep.h"
#include "pedometer.h"
#include "debug_utils.h"

struct accl_data_struct accl_data;

#ifdef USE_SC7A20

/*
   Copyright (c) 2020 Aaron Christophel

   SPDX-License-Identifier: GPL-3.0-or-later
*/

#define SC_ADDR 			0x18
#define SC_WHO_AM_I 		0x0F

#define SC_CTRL_REG1 		0x20
#define SC_CTRL_REG3		0x22
#define SC_CTRL_REG4 		0x23
#define SC_CTRL_REG5 		0x24
#define SC_CTRL_REG6		0x25
#define SC_STATUS_REG 		0x27

#define SC_FIFO_CTRL_REG	0x2E
#define SC_FIFO_SRC_REG 	0x2F

#define SC_OUT_X_L 		0x28
#define SC_OUT_X_H 		0x29
#define SC_OUT_Y_L 		0x2A
#define SC_OUT_Y_H 		0x2B
#define SC_OUT_Z_L 		0x2C
#define SC_OUT_Z_H 		0x2D

#define SC_INT1_SRC 	0x31
#define SC_INT1_CFG		0x30

#define SC_INT2_SRC 	0x35
#define SC_INT2_CFG		0x34

#define SC_TEMPCFG 		0x1F
#define SC_OUT_TEMP_L 	0x0C
#define SC_OUT_TEMP_H 	0x0D

#define SC_ODR_10			0x20
#define SC_ODR_25			0x30
#define SC_LP_EN			3
#define	SC_Z_EN				2
#define	SC_Y_EN				1
#define	SC_X_EN				0

#define SC_STATUS_ZYXDA		3

#define SC_BDU				7
#define SC_STATUS_ZYXDA		3

#define SC_I1_ZYXDA			4
#define SC_I1_WTM			2
#define SC_I2_IA1			6

#define SC_FIFO_EN			6

#define SC_LOW_POWER		0
#define SC_USE_INT1			0
#define SC_FIFO				0

#define SC_DR_25			1	
#define SC_POLL_TIME        40			


bool int1_drdy = true;

static void init_ctrl_reg(uint8_t reg,uint8_t val)
{
	switch (reg) {
		case SC_CTRL_REG1:{
			accl_write_reg(SC_CTRL_REG1, val);
			break;
		}
		case SC_CTRL_REG3:{
			accl_write_reg(SC_CTRL_REG3, val);
			break;
		}
		case SC_CTRL_REG4:{
			accl_write_reg(SC_CTRL_REG4, val);
			break;
		}
		case SC_CTRL_REG5:{
			accl_write_reg(SC_CTRL_REG5, val);
			break;
		}
		default:{
			break;
		}
	}
}

void init_accl() {
    pinMode(BMA421_INT, INPUT);
	
	// wait for boot sequence to complete
	delay(10);

	uint8_t ctrl1 = 0, ctrl3 = 0, ctrl4 = 0, ctrl5 = 0;

	init_ctrl_reg(SC_CTRL_REG4, ctrl4); // Clear HR bit first

	#if SC_DR_25
	ctrl1 |= SC_ODR_25;
	#else
	ctrl1 |= SC_ODR_10;
	#endif
	ctrl1 |= (1 << SC_Z_EN) | (1 << SC_Y_EN) | (1 << SC_X_EN);
	#if SC_LOW_POWER
	ctrl1 |= (1 << SC_LP_EN);
	#endif
	init_ctrl_reg(SC_CTRL_REG1, ctrl1);

	#if SC_USE_INT1
	#if SC_FIFO
	ctrl3 |= (1 << SC_I1_WTM);
	init_ctrl_reg(SC_CTRL_REG3, ctrl3);
	#else
	ctrl3 |= (1 << SC_I1_ZYXDA) | (1 << 3);
	init_ctrl_reg(SC_CTRL_REG3, ctrl3);
	#endif
	#elif SC_USE_INT2
	ctrl3 |= (1 << SC_I1_ZYXDA);// enable DRDY interrupt
	init_ctrl_reg(SC_CTRL_REG3, ctrl3);
	uint8_t ctrl6 = 0;
	ctrl6 |= (1 << SC_I2_IA1);// Route interrupt1 to interrupt 2
	init_ctrl_reg(SC_CTRL_REG6, ctrl6);
	#endif
	

	#if SC_USE_INT1
	init_ctrl_reg(SC_INT1_CFG, 0x00);
	#elif SC_USE_INT2
	init_ctrl_reg(SC_INT2_CFG, 0x00);
	#endif

	#if SC_FIFO
	ctrl5 |= (1 << SC_FIFO_EN);
	#endif
	init_ctrl_reg(SC_CTRL_REG5, ctrl5);

	// Get device ID.
    accl_data.device_id = accl_read_reg(SC_WHO_AM_I);

	pedometer_init();
}

void reset_accl() {
	;;
}

void reset_step_counter() {
	pedometer_reset_steps();
	accl_data.steps = 0;
}

int last_y_acc = 0;
bool acc_input()
{
	if ((accl_data.x + 335) <= 670 && accl_data.z < 0)
	{
		if (!get_sleep())
		{
			if (accl_data.y <= 0)
			{
				return false;
			}
			else
			{
				last_y_acc = 0;
				return false;
			}
		}
		if (accl_data.y >= 0)
		{
			last_y_acc = 0;
			return false;
		}
		if (accl_data.y + 230 < last_y_acc)
		{
			last_y_acc = accl_data.y;
			return true;
		}
	}
	return false;
}

bool get_is_looked_at() {
	if ((accl_data.y + 300) <= 600 && (accl_data.x + 300) <= 600 && accl_data.z <= 100){
		return true;
	}
	
	return false;
}

accl_data_struct get_accl_data() {
	accl_data.steps = pedometer_get_steps();
	return accl_data;
}

void update_accl_data() {
	
	static uint32_t last_poll_millis = 0;

	if((millis() - last_poll_millis) > SC_POLL_TIME){
		if(accl_read_reg(SC_STATUS_REG) & (1 << SC_STATUS_ZYXDA)){
			int1_drdy = true;
		}
	}

    // check status
    if (int1_drdy){
		#if SC_LOW_POWER
        accl_data.x = (int8_t)accl_read_reg(SC_OUT_X_H);
        accl_data.y = (int8_t)accl_read_reg(SC_OUT_Y_H);
        accl_data.z = (int8_t)accl_read_reg(SC_OUT_Z_H);
		// convert to mg
		accl_data.x *= 16;
		accl_data.y *= 16;
		accl_data.z *= 16;
		#else
		uint8_t lsb = 0;
		lsb = accl_read_reg(SC_OUT_X_L);
        accl_data.x = (((uint16_t)accl_read_reg(SC_OUT_X_H)) << 8) | lsb;
        lsb = accl_read_reg(SC_OUT_Y_L);
        accl_data.y = (((uint16_t)accl_read_reg(SC_OUT_Y_H)) << 8) | lsb;
        lsb = accl_read_reg(SC_OUT_Z_L);
        accl_data.z = (((uint16_t)accl_read_reg(SC_OUT_Z_H)) << 8) | lsb;

        accl_data.x >>= 6;
		accl_data.y >>= 6;
        accl_data.z >>= 6;

		accl_data.x *= 4;
		accl_data.y *= 4;
		accl_data.z *= 4;
		#endif

		pedometer_calculate(accl_data.x, accl_data.y, accl_data.z);

		last_poll_millis = millis();
		int1_drdy = false;
    }
}

void accl_write_reg(uint8_t reg, uint8_t data) {
  user_i2c_write(SC_ADDR, reg, &data, 1);
}

uint8_t accl_read_reg(uint8_t reg) {
  uint8_t data;
  user_i2c_read(SC_ADDR, reg, &data, 1);
  return data;
}

void get_accl_int()
{
	// BLE_DEBUG_PRINT("SC_I1");
	int1_drdy = true;
}


#else 
bool accl_is_enabled;
static uint8_t dev_addr = BMA4_I2C_ADDR_PRIMARY;
struct bma4_dev bma;
struct bma4_accel_config accel_conf;

void init_accl()
{
	pinMode(BMA421_INT, INPUT);

	uint16_t rslt = 0;
	uint8_t init_seq_status = 0;

	watchdog_feed();

	bma.intf = BMA4_I2C_INTF;
	bma.bus_read = user_i2c_read;
	bma.bus_write = user_i2c_write;
	bma.variant = BMA42X_VARIANT;
	bma.intf_ptr = &dev_addr;
	bma.delay_us = user_delay;
	bma.read_write_len = 8;

	accel_conf.odr = BMA4_OUTPUT_DATA_RATE_100HZ;
	accel_conf.range = BMA4_ACCEL_RANGE_2G;
	accel_conf.bandwidth = BMA4_ACCEL_NORMAL_AVG4;
	accel_conf.perf_mode = BMA4_CIC_AVG_MODE;

	unsigned int init_counter = 0;
	do
	{
		watchdog_feed();
		rslt = rslt | do_accl_init();
		if (rslt == 0)
		{
			accl_is_enabled = true;
			accl_data.result = rslt;
			accl_data.enabled = true;
			return;
		}
	} while (init_counter++ < 5);

	accl_is_enabled = false;
	accl_data.result = rslt;
	accl_data.enabled = false;
}

uint16_t do_accl_init()
{
	uint16_t init_rslt = 0;
	reset_accl();
	delay(100);
	init_rslt = init_rslt | bma423_init(&bma);
	delay(20);
	init_rslt = init_rslt | bma423_write_config_file(&bma);
	delay(20);
	init_rslt = init_rslt | bma4_set_accel_enable(1, &bma);
	delay(20);
	init_rslt = init_rslt | bma4_set_accel_config(&accel_conf, &bma);
	delay(20);
	init_rslt = init_rslt | bma423_feature_enable(BMA423_STEP_CNTR | BMA423_STEP_ACT, 1, &bma); //Step Counter and Acticity Feature (Standing, Walking, Running)
	delay(20);
	//init_rslt = init_rslt | bma423_map_interrupt(BMA4_INTR1_MAP,  BMA423_ACTIVITY_INT | BMA423_STEP_CNTR_INT, 1,&bma);
	delay(20);
	//init_rslt = init_rslt | bma423_step_counter_set_watermark(1, &bma);// 1*20 Steps
	delay(20);

	struct bma4_int_pin_config int_pin_config;
	int_pin_config.edge_ctrl = BMA4_LEVEL_TRIGGER;
	int_pin_config.lvl = BMA4_ACTIVE_LOW;
	int_pin_config.od = BMA4_PUSH_PULL;
	int_pin_config.output_en = BMA4_OUTPUT_ENABLE;
	int_pin_config.input_en = BMA4_INPUT_DISABLE;
	bma4_set_int_pin_config(&int_pin_config, BMA4_INTR1_MAP, &bma);

	return init_rslt;
}

void reset_accl()
{
	byte standby_value[1] = {0xB6};
	user_i2c_write(dev_addr, 0x7E, standby_value, 1);
}

void reset_step_counter()
{
	bma423_reset_step_counter(&bma);
	accl_data.steps = 0;
}

int last_y_acc = 0;
bool acc_input()
{
	if (!accl_is_enabled)
	return false;
	struct bma4_accel data;
	bma4_read_accel_xyz(&data, &bma);

	#ifdef SWITCH_X_Y // pinetime has 90° rotated Accl
	short tempX = data.x;
	data.x = data.y;
	data.y = tempX;
	#endif

	if ((data.x + 335) <= 670 && data.z < 0)
	{
		if (!get_sleep())
		{
			if (data.y <= 0)
			{
				return false;
			}
			else
			{
				last_y_acc = 0;
				return false;
			}
		}
		if (data.y >= 0)
		{
			last_y_acc = 0;
			return false;
		}
		if (data.y + 230 < last_y_acc)
		{
			last_y_acc = data.y;
			return true;
		}
	}
	return false;
}

bool get_is_looked_at()
{
	if (!accl_is_enabled)
	return false;
	struct bma4_accel data;
	bma4_read_accel_xyz(&data, &bma);

	#ifdef SWITCH_X_Y // pinetime has 90° rotated Accl
	short tempX = data.x;
	data.x = data.y;
	data.y = tempX;
	#endif

	if ((data.y + 300) <= 600 && (data.x + 300) <= 600 && data.z < 100)
	return true;
	return false;
}

accl_data_struct get_accl_data()
{
	int16_t rslt;
	struct bma4_accel data;
	if (!accl_is_enabled)
	return accl_data;
	rslt = bma4_read_accel_xyz(&data, &bma);

	#ifdef SWITCH_X_Y // pinetime has 90° rotated Accl
	short tempX = data.x;
	data.x = data.y;
	data.y = tempX;
	#endif

	accl_data.x = data.x;
	accl_data.y = data.y;
	accl_data.z = data.z;

	bma423_step_counter_output(&accl_data.steps, &bma);

	int32_t get_temp_C;
	rslt = bma4_get_temperature(&get_temp_C, BMA4_DEG, &bma);

	accl_data.temp = get_temp_C / 1000;

	bma423_read_int_status(&accl_data.interrupt, &bma);
	bma423_activity_output(&accl_data.activity, &bma);

	return accl_data;
}

void get_accl_int()
{
	if (!accl_is_enabled)
	return;
}

int8_t user_i2c_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t length, void *intf_ptr)
{
	return user_i2c_read(dev_addr, reg_addr, reg_data, length);
}

int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *reg_data, uint32_t length, void *intf_ptr)
{
	return user_i2c_write(dev_addr, reg_addr, reg_data, length);
}

void user_delay(uint32_t period_us, void *intf_ptr)
{
	delayMicroseconds(period_us);
}


#endif //P8B