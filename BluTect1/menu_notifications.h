#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"
#include "fonts.h"
#include "notifications.h"

class NotificationsScreen : public Screen
{
public:
  virtual void pre()
  {
    set_gray_screen_style();
    lv_style_copy(&main_style, lv_obj_get_style(lv_scr_act()));
    main_style.body.border.part = LV_BORDER_BOTTOM;
    main_style.body.border.color = LV_COLOR_GRAY;
    main_style.body.border.width = 1;

    main_list = lv_list_create(lv_scr_act(), NULL);
		lv_list_set_anim_time(main_list, 0);
		lv_list_set_sb_mode(main_list, LV_SB_MODE_ON);
		lv_list_set_layout(main_list, LV_LAYOUT_COL_L);
		lv_obj_set_width(main_list, 240);
		lv_obj_set_height(main_list, 240);

    lv_list_set_style(main_list, LV_LIST_STYLE_BG, &main_style);
    lv_list_set_style(main_list, LV_LIST_STYLE_BTN_PR, &main_style);
    lv_list_set_style(main_list, LV_LIST_STYLE_BTN_REL, &main_style);
    this->populate_lv_list();
  }

  virtual void main()
  {

  }

  virtual void long_click()
  {

  }

  virtual void left()
  {
    display_home();
  }

  virtual void right()
  {
    display_home();
  }

  virtual void up()
  {

  }

  virtual void down()
  {

  }

  virtual void click(touch_data_struct touch_data)
  {

  }

private:
  lv_obj_t *main_list;
  lv_style_t main_style;

  void populate_lv_list()
  {
    lv_list_clean(main_list);
    reset_notification_buffer_tail();
    for(int i=0; i<get_notification_buffer_count(); ++i){
      char temp_n[64];
      memset(temp_n, 0x00, sizeof(temp_n));
      if(!get_notification_from_buffer(temp_n)){
        memcpy(temp_n, "Err..", sizeof("Err.."));
      }
      lv_list_add_btn(main_list, NULL, temp_n);
    }
  }
};

NotificationsScreen notificationsScreen;