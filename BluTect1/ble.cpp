#include "ble.h"
#include "pinout.h"
#include <BLEPeripheral.h>
#include "sleep.h"
#include "heartrate.h"
#include "time.h"
#include "battery.h"
#include "inputoutput.h"
#include "backlight.h"
#include "bootloader.h"
#include "push.h"
#include "accl.h"
#include "menu.h"
#include "display.h"
#include "ble_central.h"
#include "antiloss.h"

BLEPeripheral blePeripheral = BLEPeripheral();
BLEService main_service = BLEService("190A");
BLECharacteristic TXchar = BLECharacteristic("0002", BLENotify, 20);
BLECharacteristic RXchar = BLECharacteristic("0001", BLEWriteWithoutResponse, 20);

struct main_ble_connection_monitor_t{
	byte connected;
	unsigned long monitor_millis;
	bool enabled;
};

struct main_ble_connection_monitor_t main_ble_connection_monitor = {ble_ack_disconn_notification, millis(), true};

uint32_t connect_timeout_millis = millis();

uint32_t ble_event_buf[BLE_STACK_EVT_MSG_BUF_SIZE] __attribute__((__aligned__(BLE_EVTS_PTR_ALIGNMENT)));
uint16_t ble_event_len = sizeof(ble_event_buf);

void init_ble()
{
	blePeripheral.setLocalName("BluTect 1");
	blePeripheral.setAdvertisingInterval(500);
	blePeripheral.setDeviceName("BluTect 1");
	blePeripheral.setAdvertisedServiceUuid(main_service.uuid());
	blePeripheral.addAttribute(main_service);
	blePeripheral.addAttribute(TXchar);
	blePeripheral.addAttribute(RXchar);
	RXchar.setEventHandler(BLEWritten, ble_written);
	blePeripheral.setEventHandler(BLEConnected, ble_ConnectHandler);
	blePeripheral.setEventHandler(BLEDisconnected, ble_DisconnectHandler);

	blePeripheral.begin(); // Need to enable softdevice to add vs uuids

	ble_central_init();
	antiloss_init();
	social_distancing_init();
}

void ble_feed()
{
	memset(ble_event_buf, 0x00, sizeof(ble_event_buf));

	blePeripheral.poll(ble_event_buf, &ble_event_len);
	ble_central_loop(ble_event_buf, &ble_event_len);
	
	if(antiloss_get_notify() != ALOSS_NOTIFY){// antiloss always has priority
		sd_alert_loop();
	}

	antiloss_loop();

	switch(main_ble_connection_monitor.connected){
		case ble_disconnected:{
			if(!main_ble_connection_monitor.enabled){
				if(millis() - main_ble_connection_monitor.monitor_millis > (60000 *60)){
					set_vars_ble_connection_monitor_enabled(true);
				}
			}
			else if(millis() - main_ble_connection_monitor.monitor_millis > 10000){
				show_ble_disconnect_push();
				set_vars_ble_connected(ble_notify_disconnected);
			}
			break;
		}
		case ble_notify_disconnected:{
			show_ble_disconnect_push(false);
			break;
		}
		default:{
			break;
		}
	}
}

void ble_ConnectHandler(BLECentral &central)
{
	if(!do_not_disturb()){
		sleep_up(WAKEUP_BLECONNECTED);
	}
	set_vars_ble_connected(ble_connected);
}

void ble_DisconnectHandler(BLECentral &central)
{
	set_vars_ble_connected(ble_disconnected);
}

String answer = "";
String tempCmd = "";
int tempLen = 0, tempLen1;
boolean syn;

void ble_written(BLECentral &central, BLECharacteristic &characteristic)
{
	char remoteCharArray[22];
	tempLen1 = characteristic.valueLength();
	tempLen = tempLen + tempLen1;
	memset(remoteCharArray, 0, sizeof(remoteCharArray));
	memcpy(remoteCharArray, characteristic.value(), tempLen1);
	tempCmd = tempCmd + remoteCharArray;
	if (tempCmd[tempLen - 2] == '\r' && tempCmd[tempLen - 1] == '\n')
	{
		answer = tempCmd.substring(0, tempLen - 2);
		tempCmd = "";
		tempLen = 0;
		filterCmd(answer);
	}
}

void ble_write(String Command)
{
	Command = Command + "\r\n";
	while (Command.length() > 0)
	{
		const char *TempSendCmd;
		String TempCommand = Command.substring(0, 20);
		TempSendCmd = &TempCommand[0];
		TXchar.setValue(TempSendCmd);
		Command = Command.substring(20);
	}
}

byte get_vars_ble_connected()
{
	return main_ble_connection_monitor.connected;
}

void set_vars_ble_connected(byte state)
{
	main_ble_connection_monitor.connected = state;

	if(state == ble_disconnected || state == ble_ack_disconn_notification){
		main_ble_connection_monitor.monitor_millis = millis();
	}
}

void set_vars_ble_connection_monitor_enabled(bool enabled)
{
	main_ble_connection_monitor.enabled = enabled;
}

void filterCmd(String Command)
{
	if (Command == "AT+BOND")
	{
		ble_write("AT+BOND:OK");
	}
	else if (Command == "AT+ACT")
	{
		ble_write("AT+ACT:0");
	}
	else if (Command.substring(0, 7) == "BT+UPGB")
	{
		start_bootloader();
	}
	else if (Command.substring(0, 8) == "BT+RESET")
	{
		set_reboot();
	}
	else if (Command.substring(0, 7) == "AT+RUN=")
	{
		ble_write("AT+RUN:" + Command.substring(7));
	}
	else if (Command.substring(0, 8) == "AT+USER=")
	{
		if(Command.substring(8, 9) == "0"){
			if(Command.substring(10) == "0"){
				sd_alert_disable();
			}
			else{
				sd_alert_enable();
			}
		}
		ble_write("AT+USER:" + Command.substring(8));
	}
	else if (Command == "AT+PACE")
	{
		accl_data_struct accl_data = get_accl_data();
		ble_write("AT+PACE:" + String(accl_data.steps));
	}
	else if (Command == "AT+BATT")
	{
		ble_write("AT+BATT:" + String(get_battery_percent()));
	}
	else if (Command.substring(0, 8) == "AT+PUSH=")
	{
		ble_write("AT+PUSH:OK");
		show_push(Command.substring(8));
	}
	else if (Command == "BT+VER")
	{
		ble_write("BT+VER:" + String(BUILD_VER));
	}
	else if (Command == "AT+VER")
	{
		ble_write("BT+VER:" + String(BUILD_VER));
	}
	else if (Command == "AT+SN")
	{
		ble_write("AT+SN:Blutect1");
	}
	else if (Command.substring(0, 12) == "AT+CONTRAST=")
	{
		String contrastTemp = Command.substring(12);
		if (contrastTemp == "100"){
			sleep_up();
			set_backlight(1);
		}
		else if (contrastTemp == "175"){
			sleep_up();
			set_backlight(3);
		}
		else{
			sleep_up();
			set_backlight(7);
		}
		ble_write("AT+CONTRAST:" + Command.substring(12));
	}
	else if (Command.substring(0, 10) == "AT+MOTOR=1")
	{
		String motor_power = Command.substring(10);
		if (motor_power == "1")
		set_motor_power(50);
		else if (motor_power == "2")
		set_motor_power(200);
		else
		set_motor_power(350);
		ble_write("AT+MOTOR:1" + Command.substring(10));
		set_motor_ms();
	}
	else if (Command.substring(0, 6) == "AT+DT=")
	{
		SetDateTimeString(Command.substring(6));
		ble_write("AT+DT:" + GetDateTimeString());
	}
	else if (Command.substring(0, 5) == "AT+DT")
	{
		ble_write("AT+DT:" + GetDateTimeString());
	}
	else if (Command.substring(0, 8) == "AT+HTTP=")
	{
		show_http(Command.substring(8));
	}

	else if (Command.substring(0, 7) == "AT+TAG=")
	{
		ble_peer_device_t temp_device;
		memset(&temp_device, 0x00, sizeof(temp_device));
		temp_device.peer_address.addr_type = BLE_GAP_ADDR_TYPE_RANDOM_STATIC;
		temp_device.list_type = PEER_LIST_TYPE_PROTECT;
		String temp_str = Command.substring(7);

		if(temp_str == "DEL"){
			antiloss_set_state(ANTILOSS_STATE_DEL_DEVICE);
			return; // Exit the function
		}
		int j=0;
		uint8_t temp_address[PEER_ADDRESS_MAX_SIZE];
		for(int i=0; i<PEER_ADDRESS_MAX_SIZE; ++i){
			byte temp_c = (ascii_to_hex(temp_str.c_str()[j++]) & 0x0f);
			temp_c <<= 4;
			temp_c |= (ascii_to_hex(temp_str.c_str()[j++]) & 0x0f);

			temp_address[i] = temp_c;
			// temp_device.peer_address.addr[i] = temp_c;
		}

		for(int j=PEER_ADDRESS_MAX_SIZE-1, i=0; j>=0; --j, ++i){
			temp_device.peer_address.addr[i] = temp_address[j];
		}

		memcpy(temp_device.local_name, Command.substring(19).c_str(), Command.substring(19).length());
		temp_device.local_name_len = Command.substring(19).length();

		antiloss_add_device(&temp_device);
		antiloss_set_state(ANTILOSS_STATE_CONNECT);

		char temp_resp_buff[32];
		sprintf(temp_resp_buff, "AT+TAG:%2x%2x%2x%2x%2x%2x", temp_device.peer_address.addr[0], temp_device.peer_address.addr[1],
																temp_device.peer_address.addr[2], temp_device.peer_address.addr[3],
																temp_device.peer_address.addr[4], temp_device.peer_address.addr[5]);
		ble_write(String(temp_resp_buff));
	}

	// Wakeup alarm
	else if (Command.substring(0, 9) == "AT+ALARM=")
	{
		nwa_set_time_str(Command.substring(9));

		ble_write(("AT+ALARM:" + (Command.substring(9))));
	}

	// Display on when looked at
	else if(Command.substring(0, 10) == "AT+HANDSUP="){
		if(Command.substring(10).toInt() == 0){
			set_display_handsup(false);
		}
		else if(Command.substring(10).toInt() == 2){
			sleep_up(WAKEUP_BLEPUSH);
			set_display_handsup(true);
		}
	}

	// Process the Command to set user set heart rate measurement intervals
	else if (Command.substring(0, 14) == "AT+HRINTERVAL=")
	{
		set_timed_heartrate_interval(Command.substring(14).toInt());
		ble_write("AT+HRINTERVAL:" + Command.substring(14));
	}

	else if (Command.substring(0, 5) == "AT+HR")
	{
		if(Command.substring(5,6) == "="){
			int enable = Command.substring(6).toInt();
			heartrate_enable_monitoring(enable);
			ble_write("AT+HR:" + String(enable));
		}
		else{
			ble_write("AT+HR:" + String(get_last_heartrate()));
		}
	}
}

static uint8_t ascii_to_hex(byte c)
{
	if (c >= '0' && c <= '9')
	{
		return (byte)(c - '0');
	}
	if (c >= 'A' && c <= 'F')
	{
		return (byte)(c - 'A' + 10);
	}
	if (c >= 'a' && c <= 'f')
	{
        return (byte)(c - 'a' + 10);
	}

	return 0;
}

void ble_write_log(String logMsg)
{
	String tempmsg = "AT+LOG=";
	tempmsg += logMsg;

	ble_write(tempmsg); 
}