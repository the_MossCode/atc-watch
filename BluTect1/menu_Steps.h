#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"
#include "screen_style.h"


class StepsScreen : public Screen
{
  public:
    virtual void pre()
    {
      set_gray_screen_style();

      lv_style_copy(&steps_style, lv_obj_get_style(lv_scr_act()));
      steps_style.text.font = &mksd_medium;

      label = lv_label_create(lv_scr_act(), NULL);
      lv_label_set_text(label, "Steps");
      lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);

      label_steps = lv_label_create(lv_scr_act(), NULL);
      lv_label_set_text_fmt(label_steps, "%i", get_accl_data().steps);
      lv_obj_set_style(label_steps, &steps_style);
      lv_obj_align(label_steps, NULL, LV_ALIGN_IN_LEFT_MID, 0, 0);

      steps_icon = lv_img_create(lv_scr_act(), NULL);
      lv_img_set_src(steps_icon, &footprints);
      lv_obj_align(steps_icon, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
    }

    virtual void main()
    {
      accl_data_struct accl_data = get_accl_data();
      lv_label_set_text_fmt(label_steps, "%i", accl_data.steps);
    }

    virtual void right()
    {
      set_last_menu();
    }

  private:
    lv_obj_t *label, *steps_icon, *label_steps;
    lv_style_t steps_style;
};

StepsScreen stepsScreen;
