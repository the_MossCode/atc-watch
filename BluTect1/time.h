
#pragma once

#include "Arduino.h"
#include "debug_utils.h"

#define NWA_HR_BUFFER_SIZE    96

struct time_data_struct {
  int year;
  int month;
  int day;
  int hr;
  int min;
  int sec;
};

void init_time();
time_data_struct get_time();
void SetDateTimeString(String datetime);
void SetDate(int year, int month, int day);
void SetTime(int hr, int min);
String GetDateTimeString();

// NWA time functions
void nwa_init();
time_data_struct nwa_get_time();
void nwa_set_time_str(String timeStr);
void nwa_set_time(int hour, int min);

// Enable, disable NWA, and check whether enabled, disabled, active
void nwa_disable();
void nwa_enable();
bool nwa_is_enabled();
bool nwa_is_active();

// Acknowledge ringing alarm
void nwa_ack();

// Check whether the alarm time has been reached
bool nwa_is_ring_time();

void nwa_main_loop();

static void nwa_update_minutes_to_ring_time();

byte nwa_get_average_hr();
byte nwa_get_max_hr();
byte nwa_get_min_hr();
byte nwa_get_trig_hr();
byte nwa_get_last_hr();

byte nwa_buf_get_hr_by_index(byte index);
void nwa_buf_add_hr(byte hr);
void nwa_buf_reset();

#if DEBUG
int get_minutes_to_alarm_time_debug();
byte get_nwa_last_val_debug();
#endif //

#if (SAVE_TO_FLASH)
// Flash operations
static bool save_nwa_buf_to_flash();
static bool read_nwa_buf_from_flash();
static bool save_alarm_time_to_flash();
static bool read_alarm_time_from_flash();
static bool init_nwa_data_from_flash();
#endif // Save to flash