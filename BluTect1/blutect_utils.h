#ifndef BLUTECT_UTILS_H
#define BLUTECT_UTILS_H

#include "Arduino.h"

#define FIRMWARE_VER "210808"

uint8_t blutect_util_ascii_to_hex(uint8_t ascii_char);
float blutect_util_get_average(uint8_t *a, uint8_t len);
uint8_t blutect_util_get_max(uint8_t *a, uint8_t len);
uint8_t blutect_util_get_min(uint8_t *a, uint8_t len);

#endif