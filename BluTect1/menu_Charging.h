
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"


class ChargingScreen : public Screen
{
  public:
    virtual void pre()
    {
      set_gray_screen_style();

      label = lv_label_create(lv_scr_act(), NULL);
      lv_label_set_text(label, "Charging");
      lv_obj_align(label, NULL, LV_ALIGN_CENTER, 0, -30);

      lv_style_copy(&st, &lv_style_plain);
      st.text.color = BLUTECT_COLOR_MILK;
      st.text.font = &mksd_medium;

      label_battery_big = lv_label_create(lv_scr_act(), NULL);
      lv_obj_set_style( label_battery_big, &st );
      lv_label_set_text_fmt(label_battery_big, "%i%%", get_battery_percent());
      lv_obj_align(label_battery_big, label, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
    }

    virtual void main()
    {
      lv_label_set_text_fmt(label_battery_big, "%i%%", get_battery_percent());
    }

    virtual void long_click()
    {
      display_home();
    }

    virtual void left()
    {
      display_home();
    }

    virtual void right()
    {
      display_home();
    }

    virtual void up()
    {
      display_home();
    }
    virtual void down()
    {
      display_home();
    }

    virtual void click(touch_data_struct touch_data)
    {
      display_home();
    }

    virtual uint32_t sleepTime()
    {
      return 5000;
    }
    
  private:
    lv_obj_t *label;
    lv_obj_t *label_battery_big;
    lv_style_t st;
};

ChargingScreen chargingScreen;
