#ifndef BLE_H
#define BLE_H

// #define FIRMWARE_VER	"210110TEH"

#include "Arduino.h"
#include <BLEPeripheral.h>
#include "social_distancing.h"
#include "debug_utils.h"

enum ble_connection_state{
	ble_connected,
	ble_disconnected,
	ble_notify_disconnected,
	ble_ack_disconn_notification
};

void init_ble();
void ble_feed();
void ble_ConnectHandler(BLECentral& central);
void ble_DisconnectHandler(BLECentral& central);
void ble_DisconnectHandler(BLECentral& central);
void ble_written(BLECentral& central, BLECharacteristic& characteristic);
void ble_write(String Command);
byte get_vars_ble_connected();
void set_vars_ble_connected(byte state);
void set_vars_ble_connection_monitor_enabled(bool enabled);
void filterCmd(String Command);

static byte ascii_to_hex(byte c);

void ble_write_log(String logMsg);

#endif