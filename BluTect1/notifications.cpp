#include "notifications.h"

#include "flash.h"
#include "push.h"

#define NOTIFICATION_F_NAME         "notification.txt"
#define NOTIFICATION_BUFFER_SIZE    6

unsigned int n_buffer_head = 0, n_buffer_tail = 0;
byte n_buffer_count = 0;

bool init_notification_buffer()
{
    n_buffer_count = 0;
    n_buffer_head = 0;

    spiffs_file temp_file = flash_open_file(NOTIFICATION_F_NAME, SPIFFS_RDWR);
    if(temp_file < 0){
        temp_file = flash_open_file(NOTIFICATION_F_NAME, SPIFFS_RDWR | SPIFFS_CREAT);
        if(temp_file < 0){
            return false;
        }
    }
    flash_close_file(&temp_file);
    return true;
}

bool add_notification_to_buffer(String notification)
{
    spiffs_file flash_file = flash_open_file(NOTIFICATION_F_NAME, SPIFFS_RDWR);
    if(flash_file < 0){
        flash_file = flash_open_file(NOTIFICATION_F_NAME, SPIFFS_RDWR | SPIFFS_CREAT);
        if(flash_file < 0){
            return false;
        }
    }

    if(flash_seek_file(&flash_file, n_buffer_head) < 0){
        flash_close_file(&flash_file);
        return false;
    }

    char msg_buff[64];
    memset(msg_buff, 0x00, sizeof(msg_buff));

    byte msg_length = notification.length();
    if(msg_length >= 64){
        msg_length = 63;
    }
    memcpy(msg_buff, notification.c_str(), msg_length);

    if(flash_write_file(&flash_file, msg_buff, sizeof(msg_buff)) < 0){
        flash_close_file(&flash_file);
        return false;
    }

    flash_close_file(&flash_file);

    n_buffer_tail = n_buffer_head;
    n_buffer_head += 64;
    if(n_buffer_head  >= (NOTIFICATION_BUFFER_SIZE*64)){
        n_buffer_head = 0;
    }

    if(++n_buffer_count > NOTIFICATION_BUFFER_SIZE){
        n_buffer_count = NOTIFICATION_BUFFER_SIZE;
    }

    return true;
}

bool get_notification_from_buffer(char *notification)
{
    spiffs_file flash_file = flash_open_file(NOTIFICATION_F_NAME, SPIFFS_RDWR);
    if(flash_file < 0){
        return false;
    }

    if(flash_seek_file(&flash_file, n_buffer_tail) < 0){
        flash_close_file(&flash_file);
        return false;
    }

    if(flash_read_file(&flash_file, notification, 64) < 0){
        flash_close_file(&flash_file);
        return false;       
    }

    flash_close_file(&flash_file);

    if(n_buffer_tail == 0){
        n_buffer_tail = (NOTIFICATION_BUFFER_SIZE * 64);
    }

    n_buffer_tail -= 64;

    return true;
}

byte get_notification_buffer_count()
{
    return n_buffer_count;
}

void reset_notification_buffer_tail()
{
    if(n_buffer_count == 0){
        n_buffer_tail = 0;
    }
    else if(n_buffer_head == 0){
        n_buffer_tail = (NOTIFICATION_BUFFER_SIZE * 64) - 64;
    }
    else{
        n_buffer_tail = n_buffer_head - 64;
    }
}