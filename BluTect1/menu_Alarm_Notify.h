
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

class AlarmNotifyScreen : public Screen
{
public:
  virtual void pre()
  {
    set_gray_screen_style(&mksd_medium);

		lv_style_copy(&generic_btn_style, lv_obj_get_style(lv_scr_act()));
		generic_btn_style.text.font = &lv_font_roboto_22;
    generic_btn_style.text.color = BLUTECT_COLOR_MILK; 
		generic_btn_style.body.main_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.grad_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.radius = 2;

    label_img = lv_img_create(lv_scr_act(), NULL);
    lv_img_set_src(label_img, &img_alarm);

    lv_obj_align(label_img, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);

    label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text_fmt(label, "%02u:%02u", get_time().hr, get_time().min);
    lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
    lv_obj_align(label, label_img, LV_ALIGN_OUT_BOTTOM_MID, 0, 5);

    snooze_btn = lv_btn_create(lv_scr_act(), NULL);
    lv_btn_set_style(snooze_btn, LV_BTN_STYLE_REL, &generic_btn_style);
    lv_obj_set_event_cb(snooze_btn, lv_event_handler);
    lv_obj_align(snooze_btn, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -10);

    snooze_btn_label = lv_label_create(snooze_btn, NULL);
    lv_label_set_align(snooze_btn_label, LV_LABEL_ALIGN_CENTER);
    lv_label_set_text(snooze_btn_label, "Silence");
  }

  virtual void main()
  {
  }

  virtual void right()
  {
    display_home();
    nwa_ack();
  }

  virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
  {
    if (object == snooze_btn && event == LV_EVENT_SHORT_CLICKED)
    {
      nwa_ack();
      display_home();
    }
  }

private:
  lv_obj_t *label, *label_img;
  // lv_style_t time_style;

  lv_obj_t *snooze_btn, *snooze_btn_label;
};

AlarmNotifyScreen alarmNotifyScreen;
