#include "debug_utils.h"

#if (BLE_DEBUG)

uint32_t last_res = NRF_SUCCESS;
int last_op = 0xff;
uint8_t custom_data[20];

void ble_debug_update_vars(int op, uint32_t res, uint8_t *_custom_data)
{
    last_op = op;
    last_res = res;
    if(_custom_data != NULL){
        memcpy(custom_data, _custom_data, sizeof(custom_data));
    }
}

void ble_debug_get_vars(int *op, uint32_t *res, uint8_t *_custom_data)
{
    *op = last_op;
    *res = last_res;
    _custom_data = custom_data;
}
#endif