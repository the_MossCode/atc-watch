#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

const lv_point_t tile_view_points[] = {
	{0, 0}, {1, 0}, {2, 0}
};

class AlarmScreen : public Screen
{
	public:
	virtual void pre()
	{
		set_gray_screen_style();

		lv_style_copy(&global_screen_style,lv_obj_get_style(lv_scr_act()));
		global_screen_style.body.main_color = LV_COLOR_BLACK;
		global_screen_style.body.grad_color = LV_COLOR_BLACK;
		global_screen_style.text.color = BLUTECT_COLOR_MILK;

		lv_style_copy(&generic_btn_style, lv_obj_get_style(lv_scr_act()));
		generic_btn_style.text.font = &mksd_medium;
		generic_btn_style.body.main_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.grad_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.radius = 2;

		updateScreen = 0;
		set_swipe_enabled(true);

		create_tile_view();
		create_tile_1();
		create_tile_2();
		create_tile_3();

		//select tile 1
		lv_tileview_set_tile_act(tile_view, 0, 0, LV_ANIM_OFF);
		current_tile = 0;

		time_data_struct time_data = nwa_get_time();

		last_nwa_state.hr = time_data.hr;
		last_nwa_state.min = time_data.min;
		last_nwa_state.en = nwa_is_enabled();
	}
	
	virtual void main()
	{
		time_data_struct time_data = nwa_get_time();
		if(updateScreen){
			if(current_tile == 0){
				if(nwa_is_enabled()){
					lv_label_set_text(label_screen, "Alarm Enabled");
				}
				else{
					lv_label_set_text(label_screen, "Alarm Disabled");
				}

				lv_label_set_text_fmt(label_time, "%02u:%02u", time_data.hr, time_data.min);
			}

			updateScreen = 0;
		}

		if(last_nwa_state.hr != time_data.hr || last_nwa_state.min != time_data.min || last_nwa_state.en != nwa_is_enabled()){
			last_nwa_state.hr = time_data.hr;
			last_nwa_state.min = time_data.min;
			last_nwa_state.en = nwa_is_enabled();

			updateScreen++;
		}
	}

	virtual void right()
	{
		if(current_tile == 0){
			set_last_menu();
		}
		else{
			current_tile--;
			lv_tileview_set_tile_act(tile_view, current_tile, 0, LV_ANIM_OFF);
		}
	}

	virtual void left()
	{
		if(++current_tile >= 3){
			current_tile = 2;
		}
		lv_tileview_set_tile_act(tile_view, current_tile, 0, LV_ANIM_OFF);
	}

	virtual uint32_t sleepTime()
	{
		return 20000;
	}

	virtual void lv_event_class(lv_obj_t * object, lv_event_t event)
	{
		;;
	}

	private:
	lv_obj_t *label_screen;
	lv_obj_t *label_time;	

	lv_obj_t *tile_view;
	lv_obj_t *tile_1, *tile_2, *tile_3;

	lv_obj_t *label_hr, *label_hr_graph;
	lv_obj_t *hr_chart;
	lv_chart_series_t *hr_series;
	lv_style_t hr_chart_style;
	uint8_t updateScreen, current_tile;

	struct last_nwa_state_struct{
		uint8_t hr;
		uint8_t min;
		bool en;
	};

	struct  last_nwa_state_struct last_nwa_state;
	

	#if DEBUG
	lv_obj_t *debug_label;
	#endif //DEBUG

	void create_tile_view()
	{
		tile_view = lv_tileview_create(lv_scr_act(), NULL);
		lv_tileview_set_valid_positions(tile_view, tile_view_points, 3);
		lv_obj_set_size(tile_view, LV_HOR_RES_MAX, LV_VER_RES_MAX);
		lv_tileview_set_anim_time(tile_view, LV_ANIM_OFF);
		lv_tileview_set_style(tile_view, LV_TILEVIEW_STYLE_MAIN, &global_screen_style);
	}

	void create_tile_1()
	{
		tile_1 = lv_obj_create(tile_view, NULL);
		lv_obj_set_size(tile_1, LV_HOR_RES, LV_VER_RES);
		lv_obj_set_pos(tile_1, 0, 0);
		lv_obj_set_style(tile_1, &global_screen_style);
		// lv_tileview_add_element(tile_view, tile_1);

		label_screen = lv_label_create(tile_1, NULL);
		lv_obj_align(label_screen, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 5);
		
		if(nwa_is_enabled()){
			lv_label_set_text(label_screen, "Alarm Enabled");
		}
		else{
			lv_label_set_text(label_screen, "Alarm Disabled");
		}

		label_time = lv_label_create(tile_1, NULL);
		lv_obj_align(label_time, NULL, LV_ALIGN_CENTER, -60, -10);
		lv_obj_set_style(label_time, &generic_btn_style);

		time_data_struct alarm_time = nwa_get_time();

		lv_label_set_text_fmt(label_time, "%02u:%02u", alarm_time.hr, alarm_time.min);
	}

	void create_tile_2()
	{
		tile_2 = lv_obj_create(tile_view, NULL);
		lv_obj_set_style(tile_2, &global_screen_style);
		lv_obj_set_size(tile_2, LV_HOR_RES, LV_VER_RES);
		lv_obj_set_pos(tile_2, LV_HOR_RES, 0);
		// lv_tileview_add_element(tile_view, tile_2);

		label_hr = lv_label_create(tile_2, NULL);
		lv_obj_align(label_hr, NULL, LV_ALIGN_IN_TOP_LEFT, 5, 10);
		lv_label_set_text_fmt(label_hr, "Last HR: %d\nAvg. HR: %d\nMax HR: %d\nMin HR: %d\nTrig. HR: %d", 
										nwa_get_last_hr(), nwa_get_average_hr(), nwa_get_max_hr(), nwa_get_min_hr(), nwa_get_trig_hr());
	}

	void create_tile_3()
	{
		tile_3 = lv_obj_create(tile_view, NULL);
		lv_obj_set_style(tile_3, &global_screen_style);
		lv_obj_set_size(tile_3, LV_HOR_RES, LV_VER_RES);
		lv_obj_set_pos(tile_3, (LV_HOR_RES_MAX + LV_HOR_RES), 0);
		// lv_tileview_add_element(tile_view, tile_3);

		label_hr_graph = lv_label_create(tile_3, NULL);
		lv_obj_align(label_hr_graph, NULL, LV_ALIGN_IN_TOP_LEFT, 5, 5);
		lv_label_set_text(label_hr_graph, "HR Graph");

		hr_chart = lv_chart_create(tile_3, NULL);
		lv_obj_set_size(hr_chart, NWA_HR_BUFFER_SIZE*2, 130);
		lv_obj_align(hr_chart, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
		lv_chart_set_type(hr_chart, LV_CHART_TYPE_COLUMN);
		lv_chart_set_div_line_count(hr_chart, NWA_HR_BUFFER_SIZE, 130);
		lv_chart_set_point_count(hr_chart, NWA_HR_BUFFER_SIZE);
		lv_chart_set_update_mode(hr_chart, LV_CHART_UPDATE_MODE_SHIFT);
		
		lv_style_copy(&hr_chart_style, &lv_style_plain);
		hr_chart_style.body.grad_color = LV_COLOR_BLACK;
		hr_chart_style.body.main_color = LV_COLOR_BLACK;
		hr_chart_style.body.border.color = LV_COLOR_GRAY;
		hr_chart_style.body.border.width = 2;
		hr_chart_style.line.color = LV_COLOR_BLACK;
		lv_chart_set_style(hr_chart, LV_CHART_STYLE_MAIN, &hr_chart_style);
		
		hr_series = lv_chart_add_series(hr_chart, LV_COLOR_YELLOW);

		update_hr_chart();
	}

	void update_hr_chart()
	{
		for(int i=0; i<NWA_HR_BUFFER_SIZE; ++i){
			hr_series->points[i] = nwa_buf_get_hr_by_index(i);
		}

		lv_chart_refresh(hr_chart);
	}
};


AlarmScreen alarmScreen;
