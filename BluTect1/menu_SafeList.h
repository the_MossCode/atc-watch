#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "push.h"
#include "flash.h"
#include "screen_style.h"

#include "social_distancing.h"
#include "touch.h"

class SafeListScreen : public Screen
{
	public:
	virtual void pre()
	{
		set_gray_screen_style();
		lv_style_copy(&global_screen_style, lv_obj_get_style(lv_scr_act()));

		set_swipe_enabled(true);
		this->createLabels();
	}

	virtual void main()
	{
	}

	virtual void right()
	{
		set_last_menu();
	}

	virtual void up()
	{
		lv_list_up(safe_list);
	}

	virtual void down()
	{
		lv_list_down(safe_list);
	}



	private:

	void createLabels()
	{
		// lv_obj_t *currLabel = NULL;
		
		safe_list = lv_list_create(lv_scr_act(), NULL);
		lv_list_set_anim_time(safe_list, 0);
		lv_list_set_sb_mode(safe_list, LV_SB_MODE_ON);
		lv_list_set_layout(safe_list, LV_LAYOUT_COL_L);
		lv_obj_set_width(safe_list, 240);
		lv_obj_set_height(safe_list, 240);
		
		lv_list_set_style(safe_list, LV_LIST_STYLE_BG, &global_screen_style);
		lv_list_set_style(safe_list, LV_LIST_STYLE_BTN_PR, &global_screen_style);
		lv_list_set_style(safe_list, LV_LIST_STYLE_BTN_REL, &global_screen_style);
		
		char localName[LOCAL_NAME_MAX_SIZE];

		for(int i=0; i<SDD_MAX_COUNT; ++i){
			memset(localName, 0x00, sizeof(localName));

			sd_alert_get_local_name(localName, i);

			if(localName[0] == 0x00){
				if(i == 0 && localName[1] == 0x00 && localName[2] == 0x00){
					label = lv_label_create(lv_scr_act(), NULL);
					lv_obj_set_style(label, &global_screen_style);
					lv_label_set_text(label, "Safe List");
					lv_label_set_align(label, LV_LABEL_ALIGN_LEFT);
					lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);
					return;
				}
				continue;
			}
			
			lv_list_add_btn(safe_list, NULL, String(localName).c_str());
		}
	}
	
	lv_obj_t *safe_list;
	lv_obj_t *label;

};

SafeListScreen safeListScreen;
