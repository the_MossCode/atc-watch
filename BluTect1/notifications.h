#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include "Arduino.h"

bool init_notification_buffer();
bool add_notification_to_buffer(String notification);
bool get_notification_from_buffer(char *notification);
byte get_notification_buffer_count();
void reset_notification_buffer_tail();

#endif // NOTIFICATIONS_H