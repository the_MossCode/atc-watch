

#include "Arduino.h"
#include "sleep.h"
#include "time.h"
#include "heartrate.h"
#include "inputoutput.h"
#include <TimeLib.h>

#include "menu.h"
#include "push.h"
#include "flash.h"
#include "ble.h"

#include "blutect_utils.h"

#define NWA_BUF_F_NAME "nwa_buf.log"

enum nwaStates
{
  NWA_DISABLED,
  NWA_SILENCED,
  NWA_RINGING,
  NWA_ACK,
  NWA_SNOOZED
};

enum ringStates
{
  NWA_RINGING_RING,
  NWA_RINGING_SNOOZE
};

typedef struct
{
  uint8_t hr;
  uint8_t min;
  int minutes_to_ring_time;
} nwa_time_t;

typedef struct
{
  byte buf_head;
  byte data_count;
  byte buf[NWA_HR_BUFFER_SIZE];
} nwa_hr_buf_t;

typedef struct
{
  uint8_t alarm_state;
  nwa_time_t set_time;
  nwa_hr_buf_t hr_buf;
  byte max_hr;
  byte min_hr;
  byte average_hr;
  byte trig_hr;
} alarm_struct_t;

time_data_struct time_data;
alarm_struct_t nwa;

static uint8_t filter_hr(uint8_t current_hr)
{
  if(nwa.hr_buf.data_count <= 0){
    return current_hr;
  }
  else{
    uint8_t prev_head = (nwa.hr_buf.buf_head == 0) ? nwa.hr_buf.data_count - 1: nwa.hr_buf.buf_head - 1;
    
    uint8_t average = (nwa_buf_get_hr_by_index(prev_head) + current_hr) * 0.5;

    return average;
  }
}

void init_time()
{
  int year = 2020;
  int month = 1;
  int day = 1;
  int hr = 0;
  int min = 0;
  int sec = 0;
  setTime(hr, min, sec, day, month, year);

  nwa_init();
}

time_data_struct get_time()
{
  time_data.year = year();
  time_data.month = month();
  time_data.day = day();
  time_data.hr = hour();
  time_data.min = minute();
  time_data.sec = second();
  return time_data;
}

void SetDateTimeString(String datetime)
{
  int year = datetime.substring(0, 4).toInt();
  int month = datetime.substring(4, 6).toInt();
  int day = datetime.substring(6, 8).toInt();
  int hr = datetime.substring(8, 10).toInt();
  int min = datetime.substring(10, 12).toInt();
  int sec = datetime.substring(12, 14).toInt();
  setTime(hr, min, sec, day, month, year);
}

void SetDate(int year, int month, int day)
{
  time_data = get_time();
  setTime(time_data.hr, time_data.min, time_data.sec, day, month, year);
}

void SetTime(int hr, int min)
{
  time_data = get_time();
  setTime(hr, min, 0, time_data.day, time_data.month, time_data.year);
}

String GetDateTimeString()
{
  String datetime = String(year());
  if (month() < 10)
    datetime += "0";
  datetime += String(month());
  if (day() < 10)
    datetime += "0";
  datetime += String(day());
  if (hour() < 10)
    datetime += "0";
  datetime += String(hour());
  if (minute() < 10)
    datetime += "0";
  datetime += String(minute());
  return datetime;
}

// NWA

void nwa_init()
{
  nwa.alarm_state = NWA_SNOOZED;
  nwa.set_time = {
      .hr = 0,
      .min = 0,
      .minutes_to_ring_time = 0};

  nwa.hr_buf.buf_head = 0;
  nwa.hr_buf.data_count = 0;
  memset(nwa.hr_buf.buf, 0x00, NWA_HR_BUFFER_SIZE);

  nwa.max_hr = 0;
  nwa.min_hr = 0;
  nwa.average_hr = 0;
  nwa.trig_hr = 0;

#if (SAVE_TO_FLASH)
  init_nwa_data_from_flash();
#endif
}

time_data_struct nwa_get_time()
{
  time_data_struct temp_time = time_data;
  temp_time.hr = nwa.set_time.hr;
  temp_time.min = nwa.set_time.min;

  return temp_time;
}

void nwa_set_time_str(String timeStr)
{
  int hr = timeStr.substring(2, 4).toInt();
  int min = timeStr.substring(4, 6).toInt();
  nwa_set_time(hr, min);

  uint8_t enable_byte = blutect_util_ascii_to_hex(timeStr.charAt(6));
  enable_byte <<= 4;
  enable_byte |= (blutect_util_ascii_to_hex(timeStr.charAt(7)));

  if(enable_byte & (1 << 0)){
    nwa_enable();
  }
  else{
    nwa_disable();
  }
}

void nwa_set_time(int hour, int min)
{
  nwa.set_time.hr = hour;
  nwa.set_time.min = min;

  nwa.alarm_state = NWA_SNOOZED;
#if SAVE_TO_FLASH
  save_alarm_time_to_flash();
#endif
}

void nwa_disable()
{
  nwa.alarm_state = NWA_DISABLED;
  // enable_do_not_disturb(false, DND_SOURCE_ALARM);
#if SAVE_TO_FLASH
  save_alarm_time_to_flash();
#endif
}

void nwa_enable()
{
  nwa.alarm_state = NWA_SNOOZED;
  nwa.trig_hr = 0;
  nwa.min_hr = 0;
  nwa.max_hr = 0;
  nwa.average_hr = 0;

  nwa_buf_reset();
#if SAVE_TO_FLASH
  save_alarm_time_to_flash();
  save_nwa_buf_to_flash();
#endif
}

bool nwa_is_active()
{
  return (nwa.alarm_state == NWA_SILENCED);
}

bool nwa_is_enabled()
{
  return nwa.alarm_state != NWA_DISABLED;
}

void nwa_ack()
{
  nwa.alarm_state = NWA_ACK;
  enable_do_not_disturb(false, DND_SOURCE_ALARM);
}

bool nwa_is_ring_time()
{
  // Inside the 1 hour ring window
  if (nwa.set_time.minutes_to_ring_time < 30 || (24 * 60) - nwa.set_time.minutes_to_ring_time < 30)
  {
    byte last_hr = nwa_get_last_hr();

    byte reference_hr = (nwa.average_hr + nwa.max_hr) / 2;
    
    #if (DEBUG)
    char debug_str[32];
    sprintf(debug_str, "L: %d, R:%d", last_hr, reference_hr);
    BLE_DEBUG_PRINT(debug_str);
    #endif

    if ((last_hr > reference_hr) && (last_hr > 0) && (last_hr < 254))
    {
      nwa.trig_hr = last_hr;
      return true;
    }
  }

  return false;
}

void nwa_main_loop()
{
  static uint8_t nwa_ring_counter = 0;
  static uint32_t check_timer = 0;

  uint32_t check_interval = millis() - check_timer;

  switch (nwa.alarm_state)
  {
  case NWA_DISABLED:
  {
    // Do nothing
    break;
  }
  case NWA_SILENCED:
  {
    // Check every minute
    if (check_interval >= 60000)
    {
      nwa_update_minutes_to_ring_time();
      if(nwa_is_ring_time() || ((24 * 60) - nwa.set_time.minutes_to_ring_time == 30)){
        nwa.alarm_state = NWA_RINGING;
        nwa_ring_counter = 0;
        // notify phone of wakeup alarm
        ble_write("AT+ALARM:001");
      }

      check_timer = millis();
    }
    break;
  }
  case NWA_RINGING:
  {
    static uint8_t nwa_ring_state = NWA_RINGING_RING;
    static uint32_t nwa_ring_timer = millis();
    if (nwa_ring_state == NWA_RINGING_RING)
    {
      if (millis() - nwa_ring_timer > 4000)
      {
        show_alarm_push(true);
        nwa_ring_timer = millis();
        if (++nwa_ring_counter >= 6)
        {
          nwa_ring_state = NWA_RINGING_SNOOZE;
        }
      }
    }
    else
    {
      if (millis() - nwa_ring_timer > (60000 * 5))
      {
        nwa_ring_timer = millis();
        nwa_ring_counter = 0;
        nwa_ring_state = NWA_RINGING_RING;
      }
    }
    break;
  }

  case NWA_ACK:
  {
    if(check_interval >= 60000){
      nwa_update_minutes_to_ring_time();
      if ((24 * 60) - nwa.set_time.minutes_to_ring_time > (3 * 60) && nwa.set_time.minutes_to_ring_time > (8 * 60))
      {
        nwa.alarm_state = NWA_SNOOZED;
      }

      check_timer = millis();
    }
    break;
  }
  default:
  {
    if(check_interval >= 60000){
      nwa_update_minutes_to_ring_time();
      if (nwa.set_time.minutes_to_ring_time <= (8 * 60) && nwa.set_time.minutes_to_ring_time > 30)
      {
        nwa_buf_reset();

        nwa.alarm_state = NWA_SILENCED;
        set_timed_heartrate_interval(5);
        enable_do_not_disturb(true, DND_SOURCE_ALARM);
      }

      check_timer = millis();
    }
    break;
  }
  }
}

static void nwa_update_minutes_to_ring_time()
{
  time_data_struct current_time = get_time();

  unsigned int curr_time_mins = (current_time.hr * 60) + current_time.min;
  unsigned int alarm_time_mins = (nwa.set_time.hr * 60) + nwa.set_time.min;

  if (alarm_time_mins >= curr_time_mins)
  {
    nwa.set_time.minutes_to_ring_time = alarm_time_mins - curr_time_mins;
  }
  else
  {
    nwa.set_time.minutes_to_ring_time = ((24 * 60) - curr_time_mins) + alarm_time_mins;
  }
}

byte nwa_get_average_hr()
{
  unsigned int temp_sum = 0, valid_entries = 0;

  for (int i = 0; i < NWA_HR_BUFFER_SIZE; ++i)
  {
    if (nwa.hr_buf.buf[i] > 0 && nwa.hr_buf.buf[i] < 254)
    {
      temp_sum += nwa.hr_buf.buf[i];
      valid_entries++;
    }
  }

  if (valid_entries > 0)
  {
    if (nwa.set_time.minutes_to_ring_time > 30 && nwa.set_time.minutes_to_ring_time < (8*60))
    {
      nwa.average_hr = temp_sum / valid_entries;
    }
  }

  return nwa.average_hr;
}

byte nwa_get_max_hr()
{
  byte temp_max = 0;
  for (int i = 0; i < NWA_HR_BUFFER_SIZE; ++i)
  {
    if (temp_max <= nwa.hr_buf.buf[i])
    {
      temp_max = nwa.hr_buf.buf[i];
    }
  }

  if (nwa.set_time.minutes_to_ring_time > 30 && nwa.set_time.minutes_to_ring_time < (8*60))
  {
    nwa.max_hr = temp_max;
  }

  return nwa.max_hr;
}

byte nwa_get_min_hr()
{
  byte temp_min = 0xff;
  for (int i = 0; i < NWA_HR_BUFFER_SIZE; ++i)
  {
    if ((temp_min >= nwa.hr_buf.buf[i]) && (nwa.hr_buf.buf[i] > 0))
    {
      temp_min = nwa.hr_buf.buf[i];
    }
  }

  if (temp_min >= 254)
  {
    temp_min = 0;
  }

  if (nwa.set_time.minutes_to_ring_time > 30 && nwa.set_time.minutes_to_ring_time < (8*60)) // Not in alarm ring window
  {
   nwa.min_hr = temp_min;
  }

  return nwa.min_hr;
}

byte nwa_get_trig_hr()
{
  return nwa.trig_hr;
}

byte nwa_get_last_hr()
{
  if (nwa.hr_buf.buf_head <= 0){
    if(nwa.hr_buf.data_count >= NWA_HR_BUFFER_SIZE){
      return nwa_buf_get_hr_by_index(NWA_HR_BUFFER_SIZE - 1);
    }
  }
  else{
    return nwa_buf_get_hr_by_index(nwa.hr_buf.buf_head - 1);
  }
}

byte nwa_buf_get_hr_by_index(byte index)
{
  return (index < NWA_HR_BUFFER_SIZE) ? nwa.hr_buf.buf[index] : 0;
}

void nwa_buf_add_hr(byte hr)
{
  if (nwa.alarm_state != NWA_DISABLED)
  {
    // nwa.hr_buf.buf[nwa.hr_buf.buf_head] = hr;
    nwa.hr_buf.buf[nwa.hr_buf.buf_head] = filter_hr(hr);

    #if (DEBUG)
    char debug_str[32];
    sprintf(debug_str, "HR: %d", nwa.hr_buf.buf[nwa.hr_buf.buf_head]);
    BLE_DEBUG_PRINT(debug_str);
    #endif

    if (++nwa.hr_buf.buf_head >= NWA_HR_BUFFER_SIZE)
    {
      nwa.hr_buf.buf_head = 0;
    }

    if (++nwa.hr_buf.data_count >= NWA_HR_BUFFER_SIZE)
    {
      nwa.hr_buf.data_count = NWA_HR_BUFFER_SIZE;
    }

    nwa_get_average_hr();
    nwa_get_max_hr();
    nwa_get_min_hr();

#if SAVE_TO_FLASH
    save_nwa_buf_to_flash();
#endif
  }
}

void nwa_buf_reset()
{
  nwa.hr_buf.buf_head = 0;
  nwa.hr_buf.data_count = 0;
  memset(nwa.hr_buf.buf, 0x00, NWA_HR_BUFFER_SIZE);
}

#if DEBUG
int get_minutes_to_alarm_time_debug()
{
  return nwa.set_time.minutes_to_ring_time;
}

byte get_nwa_last_val_debug()
{
  return nwa_get_last_hr();
}
#endif //DEBUG

#if (SAVE_TO_FLASH)
// Flash Operations
static bool save_nwa_buf_to_flash()
{
  spiffs_file nwa_file = flash_open_file(NWA_BUF_F_NAME, SPIFFS_RDWR);
  if (nwa_file < 0)
  {
    BLE_DEBUG_PRINT("ERR_OPEN_NWA:" + String(flash_get_last_error()));
    return false;
  }

  bool is_op_success = true;
  if (flash_seek_file(&nwa_file, 3) < 0)
  {
    // Skip time section
    BLE_DEBUG_PRINT("ERR_SEEK_NWA:" + String(flash_get_last_error()));
    is_op_success = false;
  }

  uint8_t temp_status_buf[2];
  temp_status_buf[0] = nwa.hr_buf.buf_head;
  temp_status_buf[1] = nwa.hr_buf.data_count;

  if (is_op_success)
  {
    if (flash_write_file(&nwa_file, (const char *)temp_status_buf, 2) < 0)
    {
      BLE_DEBUG_PRINT("ERR_WR_NWA");
      is_op_success = false;
    }
  }

  if (is_op_success)
  {
    if (flash_write_file(&nwa_file, (const char *)nwa.hr_buf.buf, NWA_HR_BUFFER_SIZE) < 0)
    {
      BLE_DEBUG_PRINT("ERR_WR_NWA");
      is_op_success = false;
    }
  }

  flash_close_file(&nwa_file);

  return is_op_success;
}

static bool read_nwa_buf_from_flash()
{
  spiffs_file nwa_file = flash_open_file(NWA_BUF_F_NAME, SPIFFS_RDONLY);
  if (nwa_file < 0)
  {
    BLE_DEBUG_PRINT("ERR_OPEN_NWA:" + String(flash_get_last_error()));
    return false;
  }

  bool is_op_success = true;
  if (flash_seek_file(&nwa_file, 3) < 0)
  {
    BLE_DEBUG_PRINT("ERR_SEEK_NWA:" + String(flash_get_last_error()));
    is_op_success = false;
  }

  uint8_t temp_status_buf[2];

  if (is_op_success)
  {
    if (flash_read_file(&nwa_file, (char *)temp_status_buf, 2) < 0)
    {
      BLE_DEBUG_PRINT("ERR_RD_NWA:");
      is_op_success = false;
    }
  }

  if (is_op_success)
  {
    if (flash_read_file(&nwa_file, (char *)nwa.hr_buf.buf, NWA_HR_BUFFER_SIZE) < 0)
    {
      BLE_DEBUG_PRINT("ERR_RD_NWA");
      is_op_success = false;
    }
  }

  nwa.hr_buf.buf_head = temp_status_buf[0];
  nwa.hr_buf.data_count = temp_status_buf[1];

  flash_close_file(&nwa_file);

  return is_op_success;
}

static bool save_alarm_time_to_flash()
{
  spiffs_file nwa_file = flash_open_file(NWA_BUF_F_NAME, SPIFFS_RDWR);
  if (nwa_file < 0)
  {
    BLE_DEBUG_PRINT("ERR_OPEN_NWA:" + String(flash_get_last_error()));
    return false;
  }

  bool is_op_success = true;
  uint8_t temp_status_buf[3];
  temp_status_buf[0] = nwa.alarm_state == NWA_DISABLED ? NWA_DISABLED : NWA_SNOOZED;
  temp_status_buf[1] = nwa.set_time.hr;
  temp_status_buf[2] = nwa.set_time.min;

  if (flash_write_file(&nwa_file, (const char *)temp_status_buf, 3) < 0)
  {
    BLE_DEBUG_PRINT("ERR_WR_NWA:");
    is_op_success = false;
  }

  flash_close_file(&nwa_file);

  return is_op_success;
}

static bool read_alarm_time_from_flash()
{
  spiffs_file nwa_file = flash_open_file(NWA_BUF_F_NAME, SPIFFS_RDONLY);
  if (nwa_file < 0)
  {
    BLE_DEBUG_PRINT("ERR_OPEN_NWA:" + String(flash_get_last_error()));
    return false;
  }

  bool is_op_success = true;
  uint8_t temp_status_buf[3];
  if (flash_read_file(&nwa_file, (char *)temp_status_buf, 3) < 0)
  {
    BLE_DEBUG_PRINT("ERR_RD_NWA:");
    is_op_success = false;
  }

  nwa.alarm_state = temp_status_buf[0];
  nwa.set_time.hr = temp_status_buf[1];
  nwa.set_time.min = temp_status_buf[2];

  flash_close_file(&nwa_file);

  return is_op_success;
}

static bool init_nwa_data_from_flash()
{
  if (!read_alarm_time_from_flash())
  {
    spiffs_file nwa_file = flash_open_file(NWA_BUF_F_NAME, SPIFFS_RDWR | SPIFFS_CREAT);
    if (nwa_file < 0)
    {
      BLE_DEBUG_PRINT("ERR_OPEN_NWA:" + String(flash_get_last_error()));
      return false;
    }

    byte temp_buf[] = {nwa.alarm_state, nwa.set_time.hr, nwa.set_time.min,
                       nwa.hr_buf.buf_head, nwa.hr_buf.data_count};
    if (flash_write_file(&nwa_file, (const char *)temp_buf, sizeof(temp_buf)) < 0)
    {
      flash_close_file(&nwa_file);
      return false;
    }

    if (flash_write_file(&nwa_file, (const char *)nwa.hr_buf.buf, NWA_HR_BUFFER_SIZE) < 0)
    {
      flash_close_file(&nwa_file);
      return false;
    }

    flash_close_file(&nwa_file);
  }
  else
  {
    if (!read_nwa_buf_from_flash())
    {
      return false;
    }
  }
  return true;
}
#endif // Save to flash