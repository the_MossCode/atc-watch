#ifndef PEDOMETER_H
#define PEDOMETER_H

#include "accl.h"
#include "debug_utils.h"

void pedometer_init();
void pedometer_calculate(int16_t x, int16_t y, int16_t z);
int pedometer_get_steps();
void pedometer_reset_steps();

#if DEBUG
uint16_t pedometer_debug_get_threshold();
uint16_t pedometer_debug_get_max();
uint16_t pedometer_debug_get_min();
uint16_t pedometer_debug_get_diff();
#endif

#endif // PEDOMETER_H