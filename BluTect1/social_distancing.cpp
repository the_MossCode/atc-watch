#include "social_distancing.h"
#include "ble.h"
#include "ble_central.h"
#include "push.h"

ble_peer_device_t safeList[SDD_BUFFER_SIZE];
uint8_t sdd_buf_head, sdd_buf_count;
uint8_t sd_alert_state = OBSERVER_STATE_IDLE;

//Flash
#define SDD_FLASH_F_NAME		"devices.sd"
#define SDD_DEVICE_ENTRY_SIZE	(PEER_ADDRESS_MAX_SIZE + LOCAL_NAME_MAX_SIZE)
byte sd_f_buf_head = 0, sd_f_buf_count = 0;
spiffs_file sdd_file_handle;
// end Flash

void social_distancing_init()
{
	#if (SAVE_TO_FLASH)
	init_sdd_from_flash();
	#endif
	sd_alert_state = OBSERVER_STATE_DISABLED;
}

void sd_alert_add_device_to_safelist(ble_peer_device_t *peer_device)
{	
	safeList[sdd_buf_head] = *peer_device;

	if(++sdd_buf_head >= SDD_BUFFER_SIZE){
		sdd_buf_head = 0;
	}

	if(++sdd_buf_count >= SDD_BUFFER_SIZE){
		sdd_buf_count = SDD_BUFFER_SIZE;
	}

	if(peer_device->list_type == PEER_LIST_TYPE_SAFE){
		#if (SAVE_TO_FLASH)
		if(open_sdd_flash_file(SPIFFS_RDWR)){
			save_sdd_to_flash(peer_device);
			close_sdd_flash_file();
		}
		#endif
	}
}

void sd_alert_add_device_to_ignore_list(ble_peer_device_t *peer_device)
{
	if(sd_alert_is_device_in_ignore_list(peer_device)){
		return;
	}
	sd_alert_add_device_to_safelist(peer_device);
}

bool sd_alert_is_device_in_safelist(ble_peer_device_t *peer_device)
{
	for(int i=0; i<SDD_BUFFER_SIZE; ++i){
		if(memcmp(peer_device->peer_address.addr, safeList[i].peer_address.addr, PEER_ADDRESS_MAX_SIZE) == 0
					&& safeList[i].list_type == PEER_LIST_TYPE_SAFE){
			return true;
		}

		if(memcmp(peer_device->local_name, safeList[i].local_name, LOCAL_NAME_MAX_SIZE) == 0
					&& safeList[i].list_type == PEER_LIST_TYPE_SAFE){
			return true;
		}		
	}

	char temp_name[LOCAL_NAME_MAX_SIZE];
	uint8_t temp_addr[PEER_ADDRESS_MAX_SIZE];

	#if (SAVE_TO_FLASH)
	if(!open_sdd_flash_file(SPIFFS_RDONLY)){
		return false;
	}

	for(int i=0; i<SDD_MAX_COUNT; ++i){
		memset(temp_name, 0x00, LOCAL_NAME_MAX_SIZE);
		memset(temp_addr, 0x00, PEER_ADDRESS_MAX_SIZE);
		
		get_sdd_mac_address_from_flash(i, temp_addr);
		if(memcmp(peer_device->peer_address.addr, temp_addr, PEER_ADDRESS_MAX_SIZE) == 0){
			close_sdd_flash_file();
			return true;
		}	

		get_sdd_name_from_flash(i, temp_name);
		if(memcmp(peer_device->local_name, temp_name, PEER_ADDRESS_MAX_SIZE) == 0){
			close_sdd_flash_file();
			return true;
		}	
	}
	close_sdd_flash_file();
	#endif

	return false;
}

bool sd_alert_is_device_in_ignore_list(ble_peer_device_t *peer_device)
{
	for(int i=0; i<SDD_BUFFER_SIZE; ++i){
		if(safeList[i].list_type != PEER_LIST_TYPE_IGNORE){
			continue;
		}

		if(memcmp(peer_device->peer_address.addr, safeList[i].peer_address.addr, PEER_ADDRESS_MAX_SIZE) == 0){
			return true;
		}

		if(memcmp(peer_device->local_name, safeList[i].local_name, LOCAL_NAME_MAX_SIZE) == 0){
			return true;
		}
	}
	return false;
}

void sd_alert_get_local_name(char *localName, byte index)
{
	#if (SAVE_TO_FLASH)
	if(open_sdd_flash_file(SPIFFS_RDONLY)){
		get_sdd_name_from_flash(index, localName);
		close_sdd_flash_file();
	}
	#endif
}

void sd_alert_enable()
{
	sd_alert_state = OBSERVER_STATE_IDLE;
}

void sd_alert_disable()
{
	sd_alert_state = OBSERVER_STATE_DISABLE;
}

uint8_t sd_alert_get_state()
{
	return sd_alert_state;
}

void sd_alert_ack()
{
	sd_alert_state = OBSERVER_STATE_IDLE;
}

// void set_sd_alert_state(byte state)
// {
// 	sd_alert_state = state;
// }

void sd_alert_loop()
{
	switch (sd_alert_state) {
		case OBSERVER_STATE_IDLE:{
			if(ble_central_get_state() != ble_central_state_scan){
				ble_central_start_scan();
			}
			else if(ble_central_is_new_scanned_device()){
				scanned_device_t *scanned_device = ble_central_get_scanned_device();
				if(!sd_alert_is_device_in_safelist(&scanned_device->device) && !sd_alert_is_device_in_ignore_list(&scanned_device->device)){
					show_ble_detected(true);
					ble_central_stop_scan();// stop scan to save battery
					sd_alert_state = OBSERVER_STATE_NOTIFY;
				}
				else{
					scanned_device->is_new_device = false;// continue scanning for new devices
				}
			}
			break;
		}
		case OBSERVER_STATE_NOTIFY:{
			show_ble_detected(false);
			break;
		}
		case OBSERVER_STATE_DISABLE:{
			ble_central_stop_scan();
			sd_alert_state = OBSERVER_STATE_DISABLED;
			break;
		}
		default:{
			break;
		}
	}
}


#if (SAVE_TO_FLASH)
bool open_sdd_flash_file(spiffs_flags flags)
{
	sdd_file_handle = flash_open_file(SDD_FLASH_F_NAME, flags);
	if (sdd_file_handle < 0){
		BLE_DEBUG_PRINT("SDD_OPEN_ERR" + String(flash_get_last_error()));
		return false;
	}
	return true;	
}

bool close_sdd_flash_file()
{
	return (flash_close_file(&sdd_file_handle) == SPIFFS_OK);
}

bool get_sdd_mac_address_from_flash(byte index, byte *addr)
{
	s32_t sdd_addr = (index * SDD_DEVICE_ENTRY_SIZE) + 1;

	if(flash_seek_file(&sdd_file_handle, sdd_addr) < 0){
		BLE_DEBUG_PRINT("err_sdd_seek");
		return false;
	}

	if(flash_read_file(&sdd_file_handle, (char*)addr, PEER_ADDRESS_MAX_SIZE) < 0){
		BLE_DEBUG_PRINT("err_sdd_rd");
		return false;
	}

	return true;
}

bool get_sdd_name_from_flash(byte index, char *name)
{
	s32_t sdd_addr = (index * SDD_DEVICE_ENTRY_SIZE) + 1; 

	if(flash_seek_file(&sdd_file_handle, (sdd_addr + PEER_ADDRESS_MAX_SIZE)) < 0){
		BLE_DEBUG_PRINT("err_sdd_seek");
		return false;
	}

	if(flash_read_file(&sdd_file_handle, name, LOCAL_NAME_MAX_SIZE) < 0){
		BLE_DEBUG_PRINT("err_sdd_rd");
		return false;
	}

	return true;
}

bool get_sdd_head_from_flash()
{
	if(flash_seek_file(&sdd_file_handle, 0) < 0){
		BLE_DEBUG_PRINT("err_sdd_seek");
		return false;
	}
	if(flash_read_file(&sdd_file_handle, (char*)&sd_f_buf_head, 1) < 0){
		BLE_DEBUG_PRINT("err_sdd_rd");
		return false;
	}

	return true;
}

bool save_sdd_to_flash(ble_peer_device_t *sdd)
{
	s32_t sdd_addr = (sd_f_buf_head * SDD_DEVICE_ENTRY_SIZE) + 1;
	if(flash_seek_file(&sdd_file_handle, sdd_addr) < 0){
		BLE_DEBUG_PRINT("err_sdd_seek");
		return false;
	}

	if(flash_write_file(&sdd_file_handle, (const char*)sdd->peer_address.addr, PEER_ADDRESS_MAX_SIZE) < 0){
		BLE_DEBUG_PRINT("err_sdd_wr");
		return false;
	}

	if(flash_write_file(&sdd_file_handle, (const char*)sdd->local_name, LOCAL_NAME_MAX_SIZE) < 0){
		BLE_DEBUG_PRINT("err_sdd_rd");
		return false;
	}

	if(++sd_f_buf_head >= SDD_MAX_COUNT){
		sd_f_buf_head = 0;
	}

	if(flash_seek_file(&sdd_file_handle, 0) < 0){
		BLE_DEBUG_PRINT("err_sdd_seek_h");
		return false;
	}

	if(flash_write_file(&sdd_file_handle, (const char*)&sd_f_buf_head, 1) < 0){
		BLE_DEBUG_PRINT("err_sdd_wr_h");
		return false;
	}

	return true;	
}

bool init_sdd_from_flash()
{
	for(int i=0; i<SDD_BUFFER_SIZE; ++i){
		memset(&safeList[i].peer_address, 0x00, sizeof(ble_peer_device_t));
		memset(safeList[i].local_name, 0x00, LOCAL_NAME_MAX_SIZE);
		safeList[i].local_name_len = 0;
		safeList[i].list_type = PEER_LIST_TYPE_NONE;
	}
	sdd_buf_count = sdd_buf_head = sd_f_buf_head = 0;

	if(!open_sdd_flash_file(SPIFFS_RDONLY)){
		if(!open_sdd_flash_file(SPIFFS_RDWR | SPIFFS_CREAT)){
			return false;
		}

		if(flash_write_file(&sdd_file_handle, (const char*)&sd_f_buf_head, 1) < 0){
			BLE_DEBUG_PRINT("err_sdd_h_init");
			close_sdd_flash_file();
			return false;
		}
		for(int i=0; i<SDD_MAX_COUNT; ++i){
			if(flash_write_file(&sdd_file_handle, (const char*)safeList[0].peer_address.addr, PEER_ADDRESS_MAX_SIZE) < 0){
				close_sdd_flash_file();
				BLE_DEBUG_PRINT("err_sdd_a_init");
				return false;
			}

			if(flash_write_file(&sdd_file_handle, (const char*)safeList[0].local_name, LOCAL_NAME_MAX_SIZE) < 0){
				close_sdd_flash_file();
				BLE_DEBUG_PRINT("err_sdd_n_init");
				return false;
			}
		}
	}
	else{
		if(!get_sdd_head_from_flash()){
			close_sdd_flash_file();
			return false;
		}
		for(int i=0; i<SDD_BUFFER_SIZE; ++i){
			if(!get_sdd_name_from_flash(i, safeList[i].local_name)){
				close_sdd_flash_file();
				return false;
			}
				
			if(!get_sdd_mac_address_from_flash(i, safeList[i].peer_address.addr)){
				close_sdd_flash_file();
				return false;
			}

			safeList[i].list_type = PEER_LIST_TYPE_SAFE;
			sdd_buf_head++;
			sdd_buf_count++;
		}
	}
	close_sdd_flash_file();

	if(sdd_buf_head >= SDD_BUFFER_SIZE){
		sdd_buf_head = 0;
	}
	if(sdd_buf_count >= SDD_BUFFER_SIZE){
		sdd_buf_count = SDD_BUFFER_SIZE;
	}

	return true;
}
#endif //SAVE_TO_FLASH