#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "sleep.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"
#include "backlight.h"

#include "flash.h"

class AboutScreen : public Screen
{
public:
    virtual void pre()
    {
        label_main = lv_label_create(lv_scr_act(), NULL);
        lv_label_set_text(label_main, "Blutect 1");
        lv_obj_align(label_main, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);

        main_list = lv_list_create(lv_scr_act(), NULL);
        lv_list_set_anim_time(main_list, 0);
        lv_list_set_sb_mode(main_list, LV_SB_MODE_ON);
        lv_list_set_layout(main_list, LV_LAYOUT_COL_L);
        lv_obj_set_width(main_list, 240);
        lv_obj_set_height(main_list, 200);
        lv_obj_align(main_list, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 40);

        lv_list_set_style(main_list, LV_LIST_STYLE_BG, &global_screen_style);
        lv_list_set_style(main_list, LV_LIST_STYLE_BTN_PR, &global_screen_style);
        lv_list_set_style(main_list, LV_LIST_STYLE_BTN_REL, &global_screen_style);

        btn_uptime = lv_list_add_btn(main_list, NULL, "Uptime: ");
        label_uptime = lv_list_get_btn_label(btn_uptime);

        btn_mac_address_t = lv_list_add_btn(main_list, NULL, "Mac Address:");

        ble_gap_addr_t addr_local;
        memset(&addr_local, 0x00, sizeof(addr_local));
        sd_ble_gap_address_get(&addr_local);

        String addr_str = String(addr_local.addr[5], HEX);
        addr_str += String(":");
        addr_str += String(addr_local.addr[4], HEX);
        addr_str += String(":");
        addr_str += String(addr_local.addr[3], HEX);
        addr_str += String(":");
        addr_str += String(addr_local.addr[2], HEX);
        addr_str += String(":");
        addr_str += String(addr_local.addr[1], HEX);
        addr_str += String(":");
        addr_str += String(addr_local.addr[0], HEX);

        btn_mac_address = lv_list_add_btn(main_list, NULL, addr_str.c_str());


        btn_firmware_ver_t = lv_list_add_btn(main_list, NULL, "Firmware Ver: ");
        btn_firmware_ver = lv_list_add_btn(main_list, NULL, String(FIRMWARE_VER).c_str());
    }

    virtual void main()
    {
        long days = 0;
        long hours = 0;
        long mins = 0;
        long secs = 0;
        secs = millis() / 1000;
        mins = secs / 60;
        hours = mins / 60;
        days = hours / 24;
        secs = secs - (mins * 60);
        mins = mins - (hours * 60);
        hours = hours - (days * 24);

        char time_string[14];
        sprintf(time_string, "%i %02i:%02i:%02i", days, hours, mins, secs);

        lv_label_set_text_fmt(label_uptime, "Uptime: %s", time_string);
    }

    virtual void right()
    {
        set_last_menu();
    }

    virtual void up()
    {
        lv_list_up(main_list);
    }

    virtual void down()
    {
        lv_list_down(main_list);
    }

private:
    lv_obj_t *label_main;
    lv_obj_t *btn_firmware_ver_t, *btn_firmware_ver, *btn_mac_address_t, *btn_mac_address, *btn_uptime;
    lv_obj_t *label_uptime;

    lv_obj_t *main_list;
};

AboutScreen aboutScreen;
