
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"
#include "ble_central.h"
#include "antiloss.h"

class AntiLossScreen : public Screen
{
public:

	virtual void pre()
	{
		lv_style_copy(&red_btn_style, &lv_style_plain);
		red_btn_style.text.color = BLUTECT_COLOR_MILK;
		red_btn_style.text.font = &lv_font_roboto_22;
		red_btn_style.body.main_color = LV_COLOR_RED;
		red_btn_style.body.grad_color = LV_COLOR_RED;

		lv_style_copy(&green_btn_style, &lv_style_plain);
		green_btn_style.text.color = BLUTECT_COLOR_MILK;
		green_btn_style.text.font = &lv_font_roboto_22;
		green_btn_style.body.main_color = LV_COLOR_GREEN;
		green_btn_style.body.grad_color = LV_COLOR_GREEN;

		lv_style_copy(&black_btn_style, &green_btn_style);
		black_btn_style.text.color = BLUTECT_COLOR_MILK;
		black_btn_style.text.font = &lv_font_roboto_22;
		black_btn_style.body.main_color = LV_COLOR_BLACK;
		black_btn_style.body.grad_color = LV_COLOR_BLACK;

		lv_style_copy(&normal_btn_style, &green_btn_style);
		normal_btn_style.text.color = BLUTECT_COLOR_MILK;
		normal_btn_style.text.font = &lv_font_roboto_22;
		normal_btn_style.body.main_color = LV_COLOR_GRAY;
		normal_btn_style.body.grad_color = LV_COLOR_GRAY;

		lv_style_copy(&list_style, &lv_style_plain);
		list_style.text.color = BLUTECT_COLOR_MILK;
		list_style.text.font = &lv_font_roboto_22;
		list_style.body.main_color = LV_COLOR_BLACK;
		list_style.body.grad_color = LV_COLOR_BLACK;

		prev_state = antiloss_get_state();
		isScreenParamChanged = true;
		update_sreen(prev_state);
	}

	virtual void main()
	{
		if(prev_state != antiloss_get_state()){
			prev_state = antiloss_get_state();
			isScreenParamChanged = true;
		}

		update_sreen(prev_state);
	}

	virtual void right()
	{
		set_last_menu();
	}

	virtual void post()
	{
	}

	virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
	{
		if (event == LV_EVENT_SHORT_CLICKED)
		{
			if (object == btn1)
			{
				if (antiloss_get_state() == ANTILOSS_STATE_IDLE)
				{
					antiloss_set_notify(ALOSS_NOTIFICATION_NONE);
				}
				else if (antiloss_get_state() == ANTILOSS_STATE_CONNECTED)
				{
					if(antiloss_gatt_get_state() == antiloss_gatt_complete){
						antiloss_call_device();
					}
				}

				this->isScreenParamChanged = true;
			}
			else if (object == btn2)
			{
				antiloss_delete_device();
				isScreenParamChanged = true;
			}
		}
	}

	virtual uint32_t sleepTime()
	{
		return 30000;
	}

private:
	lv_obj_t *label, *btn1_label, *btn2_label;
	lv_obj_t *btn1, *btn2;
	lv_style_t red_btn_style, green_btn_style, list_style, black_btn_style, normal_btn_style;

	lv_obj_t *list;
	lv_obj_t *list_btn;

	uint8_t prev_state;
	bool isScreenParamChanged;

#if DEBUG
	lv_obj_t *debug_label;
#endif //DEBUG

	void createNotAssignedScreen()
	{
		lv_obj_clean(lv_scr_act());

		label = lv_label_create(lv_scr_act(), NULL);
		//lv_obj_set_width(label, 200);
		//lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
		lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
		lv_obj_align(label, NULL, LV_ALIGN_IN_LEFT_MID, 10, 0);

		lv_label_set_text(label, "Tag Not Assigned");
	}

	void createConnectingScreen()
	{
		lv_obj_clean(lv_scr_act());

		label = lv_label_create(lv_scr_act(), NULL);
		//lv_obj_set_width(label, 200);
		//lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
		lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
		lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, 40, 20);

		String labelText = String(antiloss_get_device_name());
		labelText += "\nCONNECTING...";

		lv_label_set_text(label, labelText.c_str());
	}

	void createConnectLostScreen()
	{
		lv_obj_clean(lv_scr_act());

		btn1 = lv_btn_create(lv_scr_act(), NULL);
		lv_obj_align(btn1, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 5, -5);
		lv_obj_set_event_cb(btn1, lv_event_handler);
		lv_btn_set_style(btn1, LV_BTN_STYLE_REL, &green_btn_style);
		lv_btn_set_style(btn1, LV_BTN_STYLE_INA, &normal_btn_style);
		if(antiloss_get_state() == ANTILOSS_STATE_CONNECTED){
			if(antiloss_gatt_get_state() == antiloss_gatt_complete){
				lv_btn_set_state(btn1, LV_BTN_STATE_REL);
			}
			else{
				lv_btn_set_state(btn1, LV_BTN_STATE_INA);	
			}
		}
		else{
			lv_btn_set_state(btn1, LV_BTN_STATE_INA);
		}

		btn1_label = lv_label_create(btn1, NULL);
		lv_label_set_text(btn1_label, "CALL");

		btn2 = lv_btn_create(lv_scr_act(), NULL);
		lv_obj_align(btn2, NULL, LV_ALIGN_IN_BOTTOM_RIGHT, -5, -5);
		lv_obj_set_event_cb(btn2, lv_event_handler);
		lv_btn_set_style(btn2, LV_BTN_STYLE_REL, &red_btn_style);

		btn2_label = lv_label_create(btn2, NULL);
		lv_label_set_text(btn2_label, "DEL");

		list = lv_list_create(lv_scr_act(), NULL);
		lv_obj_set_size(list, 230, 150);
		lv_obj_align(list, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);
		lv_list_set_style(list, LV_LIST_STYLE_BG, &list_style);
		lv_list_set_style(list, LV_LIST_STYLE_BTN_REL, &list_style);
	}

	void createDeletingScreen()
	{
		lv_obj_clean(lv_scr_act());

		label = lv_label_create(lv_scr_act(), NULL);
		//lv_obj_set_width(label, 200);
		//lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
		lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
		lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, 40, 20);

		String labelText = String(antiloss_get_device_name());
		labelText += "\nDELETING...";

		lv_label_set_text(label, labelText.c_str());
	}

	void update_sreen(byte aLossConnState)
	{
		if (isScreenParamChanged)
		{
			switch (aLossConnState)
			{
			case ANTILOSS_STATE_CONNECT:
			{
				createConnectingScreen();
				break;
			}
			case ANTILOSS_STATE_CONNECTED:
			{
				createConnectLostScreen();
				list_btn = lv_list_add_btn(list, &green_0, antiloss_get_device_name());
				break;
			}
			case ANTILOSS_STATE_LOST://fall through
			case ANTILOSS_STATE_DISCONNECTED:
			{
				createConnectLostScreen();
				list_btn = lv_list_add_btn(list, &red_x, antiloss_get_device_name());
				break;
			}
			case ANTILOSS_STATE_DISCONNECT:	   //fall through
			case ANTILOSS_STATE_DEL_DEVICE:
			{
				createDeletingScreen();
				break;
			}
			default:
			{
				createNotAssignedScreen();
				break;
			}
			}
		}

		isScreenParamChanged = false;
	}
};

AntiLossScreen antiLossScreen;
