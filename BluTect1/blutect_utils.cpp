#include "blutect_utils.h"

uint8_t blutect_util_ascii_to_hex(uint8_t ascii_char)
{
	if (ascii_char >= '0' && ascii_char <= '9')
	{
		return (uint8_t)(ascii_char - '0');
	}
	if (ascii_char >= 'A' && ascii_char <= 'F')
	{
		return (uint8_t)(ascii_char - 'A' + 10);
	}
	if (ascii_char >= 'a' && ascii_char <= 'f')
	{
        return (uint8_t)(ascii_char - 'a' + 10);
	}

	return 0;    
}

float blutect_util_get_average(uint8_t *a, uint8_t len)
{
    float temp_sum = 0;

    for(int i=0; i<len; ++i){
        temp_sum += a[i];
    }

    float average = temp_sum / len;

    return average;
}

uint8_t blutect_util_get_max(uint8_t *a, uint8_t len)
{
    uint8_t temp_max = 0;

    for(int i=0; i<len; ++i){
        if(a[i] > temp_max){
            temp_max = a[i];
        }
    }

    return temp_max;
}

uint8_t blutect_util_get_min(uint8_t *a, uint8_t len)
{
    if(len == 0){
        return 0;
    }
    
    uint8_t temp_min = 0xff;

    for(int i=0; i<len; ++i){
        if(a[i] < temp_min){
            temp_min = a[i];
        }
    }

    return temp_min;
}