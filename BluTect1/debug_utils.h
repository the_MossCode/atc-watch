#ifndef BLE_DEBUG_H
#define BLE_DEBUG_H

#include "ble.h"


#define DEBUG       0
#if (DEBUG)
#define BLE_DEBUG   1
#else
#define BLE_DEBUG   0
#endif //DEBUG

#define SAVE_TO_FLASH 1
#define SHOW_NOTIFICATION 1

#if (BLE_DEBUG)
#define BLE_DEBUG_PRINT(msg) ble_write_log(msg)

void ble_debug_update_vars(int op, uint32_t res, uint8_t *_custom_data);
void ble_debug_get_vars(int *op, uint32_t *res, uint8_t *_custom_data);

#else
#define BLE_DEBUG_PRINT(msg) 

#endif//BLE_DEBUG

#endif//BLE_DEBUG_H