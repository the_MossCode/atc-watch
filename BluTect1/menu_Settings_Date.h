
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

const char *month = "Jan\nFeb\nMar\nApr\nMay\nJune\nJuly\nAug\nSep\nOct\nNov\nDec";
const char *date = "01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n"
                    "11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n"
                    "21\n22\n23\n24\n25\n26\n27\n28\n29\n30\n"
                    "31";
const char *year = "2020\n2021\n2022\n2023\n2024\n2025\n2026\n2027\n2028\n2029\n2030";

class SettingsDateScreen : public Screen
{
public:
  virtual void pre()
  {
    set_gray_screen_style();
		lv_style_copy(&generic_btn_style, lv_obj_get_style(lv_scr_act()));
		generic_btn_style.body.main_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.grad_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.radius = 2;

		lv_style_copy(&pressed_btn_style, &generic_btn_style);
		pressed_btn_style.body.grad_color = LV_COLOR_BLACK;
		pressed_btn_style.body.border.color = BLUTECT_COLOR_GRAY;
		pressed_btn_style.body.border.width = 1;    
    
    set_swipe_enabled(true);

    label_screen = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_screen, "Set Date");
    lv_obj_align(label_screen, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);

    btn1 = lv_btn_create(lv_scr_act(), NULL);
    lv_obj_set_event_cb(btn1, lv_event_handler);
    lv_obj_align(btn1, NULL, LV_ALIGN_IN_BOTTOM_MID, -55, 0);
    lv_btn_set_fit2(btn1, LV_FIT_NONE, LV_FIT_TIGHT);

    lv_btn_set_style(btn1, LV_BTN_STYLE_REL, &generic_btn_style);
    lv_btn_set_style(btn1, LV_BTN_STYLE_PR, &pressed_btn_style);

    btn1_label = lv_label_create(btn1, NULL);
    lv_label_set_text(btn1_label, "Abort");

    btn2 = lv_btn_create(lv_scr_act(), NULL);
    lv_obj_set_event_cb(btn2, lv_event_handler);
    lv_btn_set_fit2(btn2, LV_FIT_NONE, LV_FIT_TIGHT);
    lv_obj_align(btn2, NULL, LV_ALIGN_IN_BOTTOM_MID, 55, 0);

    lv_btn_set_style(btn2, LV_BTN_STYLE_REL, &generic_btn_style);
    lv_btn_set_style(btn2, LV_BTN_STYLE_PR, &pressed_btn_style);

    btn2_label = lv_label_create(btn2, NULL);
    lv_label_set_text(btn2_label, "Save");

    roller1 = lv_roller_create(lv_scr_act(), NULL);
    lv_roller_set_style(roller1, LV_ROLLER_STYLE_BG, &generic_btn_style);
    lv_roller_set_style(roller1, LV_ROLLER_STYLE_SEL, &pressed_btn_style);
    lv_roller_set_options(roller1, month, LV_ROLLER_MODE_INIFINITE);
    lv_roller_set_visible_row_count(roller1, 4);
    lv_obj_align(roller1, NULL, LV_ALIGN_CENTER, -14, -15);

    roller = lv_roller_create(lv_scr_act(), NULL);
    lv_roller_set_style(roller, LV_ROLLER_STYLE_BG, &generic_btn_style);
    lv_roller_set_style(roller, LV_ROLLER_STYLE_SEL, &pressed_btn_style);
    lv_roller_set_options(roller, date, LV_ROLLER_MODE_INIFINITE);
    lv_roller_set_visible_row_count(roller, 4);
    lv_obj_align(roller, roller1, LV_ALIGN_OUT_LEFT_MID, -5, 0);

    roller2 = lv_roller_create(lv_scr_act(), NULL);
    lv_roller_set_style(roller2, LV_ROLLER_STYLE_BG, &generic_btn_style);
    lv_roller_set_style(roller2, LV_ROLLER_STYLE_SEL, &pressed_btn_style);
    lv_roller_set_options(roller2, year, LV_ROLLER_MODE_INIFINITE);
    lv_roller_set_visible_row_count(roller2, 4);
    lv_obj_align(roller2, roller1, LV_ALIGN_OUT_RIGHT_MID, 5, 0);

    time_data_struct time_data = get_time();
    lv_roller_set_selected(roller, (time_data.day - 1), LV_ANIM_OFF);
    lv_roller_set_selected(roller1, (time_data.month - 1), LV_ANIM_OFF);
    lv_roller_set_selected(roller2, (time_data.year - 2020), LV_ANIM_OFF);
  }

  virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
  {
    if (object == btn1 && event == LV_EVENT_SHORT_CLICKED)
    {
      set_last_menu();
    }
    else if (object == btn2 && event == LV_EVENT_SHORT_CLICKED)
    {
      int roller_day = lv_roller_get_selected(roller) + 1;
      int roller_month = lv_roller_get_selected(roller1) + 1;
      int roller_year = lv_roller_get_selected(roller2) + 2020;
      SetDate(roller_year, roller_month, roller_day);
      set_last_menu();
    }
  }

private:
  lv_obj_t *label_screen;
  lv_obj_t *btn1, *btn2, *btn1_label, *btn2_label;
  lv_obj_t *roller, *roller1, *roller2;

  // lv_style_t btn_style;
};

SettingsDateScreen settingsDateScreen;
