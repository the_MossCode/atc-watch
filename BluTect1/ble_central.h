
#ifndef BLE_CENTRAL_H
#define BLE_CENTRAL_H

#include <Arduino.h>
#include <BLECentralRole.h>
#include "ble_peer_common.h"

enum {
	ble_central_state_init,
	ble_central_state_idle,
	ble_central_state_scan,
}ble_central_states;

typedef struct{
    ble_peer_device_t device;
    bool is_new_device;
}scanned_device_t;

void ble_central_init();

void ble_central_loop(uint32_t *buf, uint16_t *buf_len);
void ble_central_add_remote_att(BLERemoteAttribute &att);

uint32_t ble_central_connect(ble_peer_device_t &device);
uint32_t ble_central_disconnect(ble_peer_device_t *device);
uint32_t ble_central_cancel_connect();

void ble_central_start_scan();
void ble_central_stop_scan();
bool ble_central_is_new_scanned_device();
scanned_device_t *ble_central_get_scanned_device();
void ble_central_reset_scan();

uint8_t ble_central_get_state();

void ble_central_scan_callback(ble_gap_addr_t* gap_addr, uint8_t* data, uint8_t dataLen, int8_t rssi);
void ble_central_connected_cb(ble_gap_addr_t* peerAddress);
void ble_central_disconnected_cb(uint8_t reason);
void ble_central_timeout_cb(uint8_t source);
void ble_central_role_char_updated_cb(BLECentral& central, BLERemoteCharacteristic& chr);
void ble_central_att_discovery_complete_cb();

#endif