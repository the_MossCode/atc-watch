#ifndef ACCL_H
#define ACCL_H

// #define P8B			1

#include "Arduino.h"

struct accl_data_struct {
  int16_t x;
  int16_t y;
  int16_t z;
  int32_t temp;
  uint32_t steps;
  uint16_t result;
  uint8_t activity;
  uint16_t interrupt;
  bool enabled;

  uint8_t device_id;
};

#ifdef USE_SC7A20

void init_accl();
void accl_config_read_write(bool rw, uint8_t addr, uint8_t *data, uint32_t len, uint32_t offset);
void reset_accl();
void reset_step_counter();
uint32_t read_step_data();
bool acc_input();
bool get_is_looked_at();
accl_data_struct get_accl_data();
void update_accl_data();
void accl_write_reg(uint8_t reg, uint8_t data);
uint8_t accl_read_reg(uint8_t reg);
void get_accl_int();

#else
#include "bma423.h"

void init_accl();
uint16_t do_accl_init();
void reset_accl();
void reset_step_counter();
bool acc_input();
bool get_is_looked_at();
accl_data_struct get_accl_data();
void get_accl_int();
int8_t user_i2c_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t length, void *intf_ptr);
int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *reg_data, uint32_t length, void *intf_ptr);
void user_delay(uint32_t period_us, void *intf_ptr);

#endif //P8B

#endif //ACCL_H
