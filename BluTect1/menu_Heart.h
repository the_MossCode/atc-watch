
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "inputoutput.h"
#include "heartrate.h"
#include "flash.h"

class HeartScreen : public Screen
{
	public:
	
	virtual void pre()
	{	
		set_gray_screen_style(&lv_font_roboto_28);
		
		label_hr = lv_label_create(lv_scr_act(), NULL);
		lv_label_set_align(label_hr, LV_LABEL_ALIGN_LEFT);
		lv_label_set_text_fmt(label_hr, "Average HR: %u", this->get_average_hr());
		lv_obj_align(label_hr, NULL, LV_ALIGN_IN_TOP_MID, 0, 32);

		label_hr_last = lv_label_create(lv_scr_act(), NULL);
		lv_label_set_text_fmt(label_hr_last, "%u", get_last_heartrate());
		lv_obj_align(label_hr_last, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

		create_chart();

		chart_update_millis = millis();

		start_hrs3300();
	}

	virtual void main()
	{
		byte hr = get_heartrate();
		
		switch (hr) {
			case 0:{
				break;
			}
			case 255:{
				break;
			}
			case 254:{
				lv_label_set_text_fmt(label_hr_last, "No Touch");
				break;
			}
			case 253:{
				lv_label_set_text_fmt(label_hr_last, "Wait");
				break;
			}
			default:{
				add_hr_to_buffer(hr);
				lv_label_set_text_fmt(label_hr, "Average HR: %u", this->get_average_hr());
				lv_label_set_text_fmt(label_hr_last, "%u", hr);
				break;
			}
		}
		if(millis() - chart_update_millis > 2000){
			update_chart();
			chart_update_millis = millis();
		}
	}

	virtual void post()
	{
		end_hrs3300();
	}

	virtual uint32_t sleepTime()
	{
		return 50000;
	}

	virtual void right()
	{
		set_last_menu();
		end_hrs3300();
	}

	private:
	lv_obj_t *label, *label_hr, *label_hr_last;
	
	lv_obj_t *chart;
	lv_chart_series_t *hrSeries;
	lv_style_t chartStyle;

	unsigned long chart_update_millis;
	
	void create_chart()
	{
		chart = lv_chart_create(lv_scr_act(), NULL);
		lv_obj_set_size(chart, HR_BUFFER_SIZE*2, 150);
		lv_obj_align(chart, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
		lv_chart_set_type(chart, LV_CHART_TYPE_COLUMN);
		lv_chart_set_div_line_count(chart, HR_BUFFER_SIZE, 150);
		lv_chart_set_point_count(chart, HR_BUFFER_SIZE);
		lv_chart_set_update_mode(chart, LV_CHART_UPDATE_MODE_SHIFT);
		
		lv_style_copy(&chartStyle, &lv_style_plain);
		chartStyle.body.grad_color = LV_COLOR_BLACK;
		chartStyle.body.main_color = LV_COLOR_BLACK;
		chartStyle.line.color = LV_COLOR_BLACK;
		
		lv_obj_set_style(chart, &chartStyle);
		lv_chart_set_style(chart, LV_CHART_STYLE_MAIN, &chartStyle);
		
		hrSeries = lv_chart_add_series(chart, LV_COLOR_YELLOW);
		
		update_chart();
	}
	
	void update_chart()
	{
		for(int i=0; i<HR_BUFFER_SIZE; ++i){
			hrSeries->points[i] = (get_last_heartrate_data_by_index(i) - 40);
		}
		
		lv_chart_refresh(chart);
	}
	
	uint8_t get_average_hr()
	{
		uint16_t total = 0;
		uint8_t data_points = 0;
		
		for(int i=0; i<HR_BUFFER_SIZE; ++i){
			total += get_last_heartrate_data_by_index(i);
			
			if(get_last_heartrate_data_by_index(i) > 0){
				data_points++;
			}
		}
		
		if(data_points == 0){
			return 0;
		}
		else{
			return (uint8_t)(total/data_points);
		}
	}
};

HeartScreen heartScreen;
