
#pragma once
#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

#include "antiloss.h"


class BLEPushScreen : public Screen
{
	public:
	virtual void pre()
	{
		set_gray_screen_style();	
		lv_style_copy(&green_btn_style, &lv_style_plain);
		green_btn_style.body.main_color = LV_COLOR_GREEN;
		green_btn_style.body.grad_color = LV_COLOR_GREEN;
		green_btn_style.body.radius = 2;
		green_btn_style.text.color = BLUTECT_COLOR_MILK;
		green_btn_style.text.font = &lv_font_roboto_22;
		
		lv_style_copy(&red_btn_style, &green_btn_style);
		red_btn_style.body.main_color = LV_COLOR_RED;
		red_btn_style.body.grad_color = LV_COLOR_RED;

		lv_style_copy(&pressed_btn_style, &green_btn_style);
		pressed_btn_style.body.main_color = BLUTECT_COLOR_GRAY;
		pressed_btn_style.body.grad_color = LV_COLOR_BLACK;
		pressed_btn_style.body.border.color = BLUTECT_COLOR_GRAY;
		pressed_btn_style.body.border.width = 1;
		
		msg_label = lv_label_create(lv_scr_act(), NULL);
		lv_label_set_align(msg_label, LV_LABEL_ALIGN_CENTER);
		lv_label_set_text(msg_label, "Phone Disconnected!");
		lv_obj_align(msg_label, NULL, LV_ALIGN_IN_TOP_MID, 0, 32);
		
		relax_btn = lv_btn_create(lv_scr_act(), NULL);
		lv_btn_set_style(relax_btn, LV_BTN_STYLE_REL, &green_btn_style);
		lv_btn_set_style(relax_btn, LV_BTN_STYLE_PR, &pressed_btn_style);
		lv_obj_align(relax_btn, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 10, -10);
		
		lv_obj_set_event_cb(relax_btn, lv_event_handler);
		
		relax_btn_label = lv_label_create(relax_btn, NULL);
		lv_label_set_align(relax_btn_label, LV_LABEL_ALIGN_LEFT);
		lv_label_set_text(relax_btn_label, "Relax");
		
		
		ok_btn = lv_btn_create(lv_scr_act(), NULL);
		lv_btn_set_style(ok_btn, LV_BTN_STYLE_REL, &red_btn_style);
		lv_btn_set_style(ok_btn, LV_BTN_STYLE_PR, &pressed_btn_style);
		lv_obj_align(ok_btn, NULL, LV_ALIGN_IN_BOTTOM_RIGHT, -10, -10);
		
		lv_obj_set_event_cb(ok_btn, lv_event_handler);
		
		ok_btn_label = lv_label_create(ok_btn, NULL);
		lv_label_set_align(ok_btn_label, LV_LABEL_ALIGN_CENTER);
		lv_label_set_text(ok_btn_label, "OK");
	}

	virtual void main()
	{
	}

	virtual void right()
	{
	}

	virtual void click(touch_data_struct touch_data)
	{
	}
	
	virtual void long_click()
	{
	}

	virtual uint32_t sleepTime()
	{
		return 20000;
	}
	
	virtual void lv_event_class(lv_obj_t * object, lv_event_t event)
	{
		if(get_vars_ble_connected() == ble_connected){
			display_home();
		}
		else if(object == relax_btn && event == LV_EVENT_SHORT_CLICKED){
			set_vars_ble_connected(ble_disconnected);
			set_vars_ble_connection_monitor_enabled(false);
			display_home();
		}
		else if(object == ok_btn && event == LV_EVENT_SHORT_CLICKED){
			set_vars_ble_connected(ble_ack_disconn_notification);
			display_home();
		}
	}


	private:
	
	lv_obj_t *relax_btn, *ok_btn;
	lv_obj_t *relax_btn_label, *ok_btn_label;
	lv_obj_t *msg_label;
};

BLEPushScreen blePushScreen;
