#include "pedometer.h"
#include <TimeLib.h>
// #include "math.h"

#define STEP_INTERVAL_MIN   200  // Shortest interval to walk one step (mS)
#define STEP_INTERVAL_MAX   2000 // Longest interval to walk one step (mS)
#define WINDOW_SAMPLES      ((STEP_INTERVAL_MAX/1000) * 25)   // Samples to reset min/max
#define FILTER_SIZE_PEDO    5 // Number of accelerometer readings to average
#define precision           50 // Lower = more sensitive
#define FOUND_OUT_STEPS     4



enum {
    pedometer_state_searching, pedometer_state_found_out
}pedometer_states;

uint16_t steps = 0;

typedef struct{
    uint16_t buf[FILTER_SIZE_PEDO];
    uint8_t head;
    uint8_t tail;
    uint8_t count;
    uint16_t sum;
}filter_t;

typedef struct{
    uint16_t new_sample;
    uint16_t old_sample;
    uint16_t max;
    uint16_t min;
    uint8_t reset_count;
}sample_data_t;

filter_t lp_filter_data;
sample_data_t sample_data = {
    .new_sample = 0,
    .old_sample = 0,
    .max = 0,
    .min = 0xffff,
    .reset_count = 0
};

uint16_t threshold = 0;

static uint16_t isqrt(uint32_t num) {
    uint16_t res = 1 << 15;
    for(int bit=15; bit >= 0; --bit){
        uint32_t res_square = (uint32_t)res * (uint32_t)res;
        if(num > res_square){
            if(bit == 0){
                break;
            }
            res |= (1 << (bit-1));
        }
        else if(num < res_square){
            res &= ~(1 << bit);
            if(bit == 0){
                break;
            }
            res |= (1 << bit - 1);
        }
        else{
            break;
        }
    }
    return res;
}

static uint16_t lowpassFilter(uint16_t sample){
    if(lp_filter_data.count++ >= FILTER_SIZE_PEDO){
        // Subtract oldest value
        lp_filter_data.sum -= lp_filter_data.buf[lp_filter_data.tail++];

        if(lp_filter_data.tail >= FILTER_SIZE_PEDO){
            lp_filter_data.tail = 0;
        }
        
        lp_filter_data.count = FILTER_SIZE_PEDO;
    }

    lp_filter_data.sum += sample;
    lp_filter_data.buf[lp_filter_data.head++] = sample;

    if(lp_filter_data.head >= FILTER_SIZE_PEDO){
        lp_filter_data.head = 0;
    }

    return (lp_filter_data.sum / lp_filter_data.count);
}

static uint16_t calculate_threshold()
{
    return (uint16_t)((sample_data.max + sample_data.min) / 2);
}

void pedometer_init()
{
    // Initialize filter params
    lp_filter_data.head = lp_filter_data.count = lp_filter_data.tail = lp_filter_data.sum = 0;
    memset(lp_filter_data.buf, 0x00, sizeof(lp_filter_data.buf));
    
    // Read initial accelerometer state and assign to various things to start
    accl_data_struct accl_data = get_accl_data();
    uint32_t mag_sqr = (
                        ((int32_t)accl_data.x * (int32_t)accl_data.x) + 
                        ((int32_t)accl_data.y * (int32_t)accl_data.y) + 
                        ((int32_t)accl_data.z * (int32_t)accl_data.z)
                        );
    uint16_t magnitude = isqrt(mag_sqr); // 3space magnitude
    
    sample_data.min = magnitude; // Minimum reading from accel in last WINDOW_INTERVAL seconds
    sample_data.max = magnitude; // Maximum reading from accel in last WINDOW_INTERVAL seconds
    sample_data.old_sample = magnitude;
    sample_data.new_sample = magnitude;
    sample_data.reset_count = 0;

    threshold = calculate_threshold();
}

void pedometer_calculate(int16_t x, int16_t y, int16_t z)
{
    uint32_t mag_sqr = (
                        ((int32_t)x * (int32_t)x) + 
                        ((int32_t)y * (int32_t)y) + 
                        ((int32_t)z * (int32_t)z)
                        );
    uint16_t sample_result = lowpassFilter(isqrt(mag_sqr));

    if(sample_data.reset_count++ >= WINDOW_SAMPLES){
        threshold = calculate_threshold();
        sample_data.min = sample_result;               // Reset min and max to
        sample_data.max = sample_result;                 // the last value read
        sample_data.reset_count = 0;                    // Note time of reset
    }
    else if(sample_result < sample_data.min){
        sample_data.min = sample_result;
    }
    else if (sample_result > sample_data.max){
            sample_data.max = sample_result;
    }

    static uint8_t pedometer_state = pedometer_state_searching;
    static uint32_t last_step_time = 0;
    static uint8_t state_steps;
    uint32_t time_since_last_step = millis() - last_step_time;

    if(time_since_last_step > STEP_INTERVAL_MAX){
        if(pedometer_state != pedometer_state_searching){
            pedometer_state = pedometer_state_searching;
        }
        state_steps = 0;
        last_step_time = millis();
    }
    else if (time_since_last_step >= STEP_INTERVAL_MIN)
    {
        if ((abs(sample_result - sample_data.old_sample)) >= precision)
        {
            sample_data.new_sample = sample_result;
            if ((sample_data.old_sample < threshold) && (sample_data.new_sample > threshold))
            {
                // It's a step!
                if (pedometer_state == pedometer_state_searching)
                {
                    state_steps++;
                    if (state_steps >= FOUND_OUT_STEPS)
                    {
                        pedometer_state = pedometer_state_found_out;
                        steps += FOUND_OUT_STEPS;
                    }
                }
                else
                {
                    steps++;
                }
                last_step_time = millis();
            }
        }
    }
    sample_data.old_sample = sample_result;
}

int pedometer_get_steps()
{
    return steps;
}

void pedometer_reset_steps()
{
    steps = 0;
}

#if DEBUG
uint16_t pedometer_debug_get_threshold()
{
    return threshold;
}

uint16_t pedometer_debug_get_max()
{
    return sample_data.max;
}

uint16_t pedometer_debug_get_min()
{
    return sample_data.min;
}

uint16_t pedometer_debug_get_diff()
{
    return (abs(sample_data.old_sample - sample_data.new_sample));
}
#endif
