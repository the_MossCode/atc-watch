/*
 * ble_peer_common.h
 *
 * Created: 25/09/2020 11:45:15
 *  Author: collo
 */ 


#ifndef BLE_PEER_COMMON_H_
#define BLE_PEER_COMMON_H_

#include <ble.h>

#define LOCAL_NAME_MAX_SIZE 	30
#define PEER_ADDRESS_MAX_SIZE	6

// list types
enum listStypes{
	PEER_LIST_TYPE_NONE, PEER_LIST_TYPE_SAFE, PEER_LIST_TYPE_IGNORE, PEER_LIST_TYPE_PROTECT
};


// Peer device structure
typedef struct{
	ble_gap_addr_t peer_address;
	char local_name[LOCAL_NAME_MAX_SIZE];
	byte local_name_len;
	byte list_type;
	int8_t rssi;
}ble_peer_device_t;

#endif /* BLE_PEER_COMMON_H_ */