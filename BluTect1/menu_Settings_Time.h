#pragma once

#include "Arduino.h"
#include "class.h"
#include "images.h"
#include "menu.h"
#include "display.h"
#include "ble.h"
#include "time.h"
#include "battery.h"
#include "accl.h"
#include "push.h"
#include "heartrate.h"

const char* mins = "00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n"
                    "20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n30\n31\n32\n33\n34\n35\n36\n37\n38\n39\n"
                    "40\n41\n42\n43\n44\n45\n46\n47\n48\n49\n50\n51\n52\n53\n54\n55\n56\n57\n58\n59";
const char* hours = "00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19"
                    "\n20\n21\n22\n23";

class SettingsTimeScreen : public Screen
{
public:
  virtual void pre()
  {
    set_gray_screen_style();
		lv_style_copy(&generic_btn_style, lv_obj_get_style(lv_scr_act()));
		generic_btn_style.body.main_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.grad_color = BLUTECT_COLOR_GRAY;
		generic_btn_style.body.radius = 2;

		lv_style_copy(&pressed_btn_style, &generic_btn_style);
		pressed_btn_style.body.grad_color = LV_COLOR_BLACK;
		pressed_btn_style.body.border.color = BLUTECT_COLOR_GRAY;
		pressed_btn_style.body.border.width = 1;    

    set_swipe_enabled(true);

    label_screen = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_screen, "Set Time");
    lv_obj_align(label_screen, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);

    label_points = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label_points, ":");
    lv_obj_align(label_points, NULL, LV_ALIGN_CENTER, 0, -15);

    btn1 = lv_btn_create(lv_scr_act(), NULL);
    lv_obj_set_event_cb(btn1, lv_event_handler);
    lv_obj_align(btn1, NULL, LV_ALIGN_IN_BOTTOM_MID, -0, 0);
    lv_btn_set_fit2(btn1, LV_FIT_NONE, LV_FIT_TIGHT);
    lv_btn_set_style(btn1, LV_BTN_STYLE_REL, &generic_btn_style);
    lv_btn_set_style(btn1, LV_BTN_STYLE_PR, &pressed_btn_style);

    btn1_label = lv_label_create(btn1, NULL);
    lv_label_set_text(btn1_label, "Save");

    roller1 = lv_roller_create(lv_scr_act(), NULL);
    lv_roller_set_style(roller1, LV_ROLLER_STYLE_BG, &generic_btn_style);
    lv_roller_set_style(roller1, LV_ROLLER_STYLE_SEL, &pressed_btn_style);
    lv_roller_set_options(roller1, mins, LV_ROLLER_MODE_INIFINITE);
    lv_roller_set_visible_row_count(roller1, 4);
    lv_obj_align(roller1, NULL, LV_ALIGN_CENTER, 30, -15);

    roller = lv_roller_create(lv_scr_act(), NULL);
    lv_roller_set_style(roller, LV_ROLLER_STYLE_BG, &generic_btn_style);
    lv_roller_set_style(roller, LV_ROLLER_STYLE_SEL, &pressed_btn_style);
    lv_roller_set_options(roller, hours,LV_ROLLER_MODE_INIFINITE);
    lv_roller_set_visible_row_count(roller, 4);
    lv_obj_align(roller, NULL, LV_ALIGN_CENTER, -30, -15);

    time_data_struct time_data = get_time();
    lv_roller_set_selected(roller, time_data.hr, LV_ANIM_OFF);
    lv_roller_set_selected(roller1, time_data.min, LV_ANIM_OFF);
  }

  virtual void right()
  {
    set_last_menu();
  }

  virtual void lv_event_class(lv_obj_t *object, lv_event_t event)
  {
    if (object == btn1 && event == LV_EVENT_SHORT_CLICKED)
    {
      int roller_hours = lv_roller_get_selected(roller);
      int roller_minutes = lv_roller_get_selected(roller1);
      SetTime(roller_hours, roller_minutes);
      set_last_menu();
    }
  }

private:
  lv_obj_t *label_screen;
  lv_obj_t *btn1, *btn1_label, *label_points;
  lv_obj_t *roller, *roller1;
};

SettingsTimeScreen settingsTimeScreen;
