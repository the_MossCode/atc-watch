    uint8_t dataIndex = 0, structDataLen = 0;

    while (dataIndex < dataLen) {
        structDataLen = data[dataIndex];
        dataIndex += 1;

        uint8_t advType = data[dataIndex];

        switch (advType) {
        case BLE_GAP_AD_TYPE_SHORT_LOCAL_NAME: {
            dataIndex += 1;
            peer_device.local_name_len = structDataLen - 1;

            for (int i=0; i<(structDataLen - 1); ++i) {
                peer_device.local_name[i] = data[dataIndex];
                dataIndex++;
            }

            break;
        }
        case BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME: {
            dataIndex += 1;
            peer_device.local_name_len = structDataLen - 1;

            for (int i=0; i<(structDataLen - 1); ++i) {
                peer_device.local_name[i] = data[dataIndex];
                dataIndex++;
            }

            break;
        }
        case BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA: {
            dataIndex += 1;
            uint16_t man_code = 0x0000;

            man_code |= data[dataIndex + 1];
            man_code <<= 8;
            man_code |= data[dataIndex];

            peer_device.man_code = man_code;

            dataIndex += (structDataLen - 1);

            break;
        }
        default: {
            dataIndex += structDataLen;
            break;
        }
        }
    }
